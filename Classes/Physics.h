#ifndef _PHYSICS_H
#define _PHYSICS_H

#include "cocos2d.h"
#include "Box2D/Box2D.h"

using namespace cocos2d;

class Physics
{
private:
    b2World *mWorld;
    Physics();
public:
    ~Physics(){}
    static Physics& GetInstance()
    {
        static Physics instance;
        return instance;
    }
    b2World* GetPhysicalWorld();
    void DeleteAllBodies();

};

#endif //_PHYSICS_H
