#ifndef _GAMELAYER_H
#define _GAMELAYER_H

#include "cocos2d.h"
#include "ManageableCamera.h"
#include "Level.h"

using namespace cocos2d;

class GameLayer: public Layer
{
protected:
    ManageableCamera* mMainCamera;                // main camera
    Layer* mMainLayer, *mHUDLayer, *mMessageLayer;
    Level* mCurrentLevel = nullptr;
public:
    void AddToMainLayer( Node* node);
    vector<Node*> mScheduledRemove;
    ManageableCamera* GetMainCamera() { return mMainCamera; }
    Level* GetCurrentLevel() { return mCurrentLevel; }
    GameLayer();
    ~GameLayer(){}
};

#endif //_GAMELAYER_H
