//
//  MyContactListener.h
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#ifndef _CONTACT_LISTENER_H
#define _CONTACT_LISTENER_H

#include "Box2D/Box2D.h"
#include <vector>
#include <algorithm>

//struct MyContact {
//    b2Fixture *fixtureA;
//    b2Fixture *fixtureB;
//    bool operator==(const MyContact& other) const
//    {
//        return (fixtureA == other.fixtureA) && (fixtureB == other.fixtureB);
//    }
//};

class MyContactListener : public b2ContactListener
{
private:
public:

    
    MyContactListener();
    ~MyContactListener();
    
	virtual void BeginContact(b2Contact* contact);
	virtual void EndContact(b2Contact* contact);
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);    
	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

//    void ProcessBeginContactList();
//    void ProcessEndContactList();
    
};

#endif
