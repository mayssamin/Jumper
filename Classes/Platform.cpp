#include "Platform.h"

Platform::Platform()
{

}

void Platform::SetBits()
{
    setTag(OT_PLATFORM);

    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();

    filter.categoryBits = OT_PLATFORM;
    filter.maskBits = OT_PLAYER;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
}

Platform* Platform::create(/*std::string info_path*/)
{
    Platform* plat = new Platform( );

    if ( plat->initWithFile( "platform.ini" ) )
    {
        plat->SetBits();
        plat->autorelease();

        return plat;
    }

    CC_SAFE_DELETE( plat );
    return NULL;
}

void Platform::PlayerJumpedOn( )
{
    PlayAnimation( "jumped");
}


//=============== Fragile Platform  =====================
FragilePlatform* FragilePlatform::create(/*std::string info_path*/)
{
    FragilePlatform* plat = new FragilePlatform( );

    if ( plat->initWithFile( "platform_fragile.ini" ) )
    {
        plat->SetBits();
        plat->autorelease();

        return plat;
    }

    CC_SAFE_DELETE( plat );
    return NULL;
}

void FragilePlatform::PlayerJumpedOn( )
{
    log("i should die!");
    //PlayAnimation( "jumped");
}
