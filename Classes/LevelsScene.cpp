#include "LevelsScene.h"
#include "Fish.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../cocos2d/cocos/platform/android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#endif

Scene* LevelsScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    auto layer = LevelsScene::create();

    layer->setTag( 100 );
    scene->addChild(layer);
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LevelsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if( !Layer::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto origin = Director::getInstance()->getVisibleOrigin();

    mListenerKeyboard = EventListenerKeyboard::create();
    mListenerKeyboard->onKeyReleased = CC_CALLBACK_2(LevelsScene::onKeyReleased, this);
    mListenerKeyboard->onKeyPressed = CC_CALLBACK_2(LevelsScene::onKeyPressed, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mListenerKeyboard, this);


    mLevels = Sprite::create();
    mLevels->initWithFile("levels.jpg");
    float ratio = visibleSize.height/mLevels->getBoundingBox().size.height;
    mLevels->setScale( ratio );
    mLevels->setPosition( visibleSize.width -
                          mLevels->getBoundingBox().size.width/2,
                          visibleSize.height/2 );
    //addChild( mLevels );

    mMainLayer = Layer::create();
    mMainLayer->setCameraMask( (unsigned short)CameraFlag::USER2 );
    this->addChild( mMainLayer );

    auto cacher = SpriteFrameCache::getInstance();
    cacher->addSpriteFramesWithFile("sprites.plist");

    float far = 3500;
    mMainCamera = ManageableCamera::create(80, (float)visibleSize.width/visibleSize.height,
                                           1.0, far);
    mMainCamera->setCameraFlag(CameraFlag::USER2);
    float camZ = 1200;
    mMainCamera->setPosition3D( /*mCurrentLevel->GetBottomObject()->getPosition3D() +
                                                            Vec3(0,1000,camZ)*/ Vec3( -400, 2000, camZ) );
    mMainCamera->lookAt(/*mCurrentLevel->GetBottomObject()->getPosition3D()+
                                            Vec3(0,1000,0)*/ Vec3( -400, 2000, 0), Vec3(0.0,1.0,0.0));
    this->addChild(mMainCamera);
    mPlatformManager = new PlatformManager();

    mKeyDown[EventKeyboard::KeyCode::KEY_UP_ARROW] = false;
    mKeyDown[EventKeyboard::KeyCode::KEY_DOWN_ARROW] = false;

    scheduleUpdate();
    return true;
}

void LevelsScene::update(float dt)
{
    ProcessKeyDown();
    mPlatformManager->Update();
}

void LevelsScene::onEnterTransitionDidFinish()
{
    InitializeObjects();
}

void LevelsScene::InitializeObjects()
{
    mCurrentLevel = Level::create();
    mCurrentLevel->setCameraMask( (unsigned short)CameraFlag::USER2 );
    mMainLayer->addChild( mCurrentLevel );
    //    auto bg = Sprite::create();
    //    bg->initWithFile("asphalt.png");
    //    bg->setPosition( visibleSize.width/2,0 );
    //    bg->setCameraMask( (unsigned short)CameraFlag::USER2 );
    //    mlayer->addChild( bg );
    mMainCamera->AlignWithObject( mCurrentLevel->GetBottomObject(),
                                  VA_INNER_BOTTOM, HA_MIDDLE );

    mPlatformManager->Restart();

    Fish* fish = Fish::create();
    float fishZ = -50;
    Rect cameraRect = mMainCamera->GetVisibleRect( fishZ );
    fish->setPosition3D( Vec3(cameraRect.origin.x + cameraRect.size.width * 1.5, 400, fishZ) );
    AddToMainLayer( fish );
    fish->Start();



//    mMainCamera->ZoomAround( fish, 1500 );
    //    Vec2 scrPos( 0,0);
    //    Vec3 pos = mMainCamera->GetScreenToWorld( scrPos );
    //    log("screen pos( %f, %f) == world pos( %f, %f, %f) ", scrPos.x, scrPos.y, pos.x, pos.y, pos.z );
    //    bg->setPosition( pos.x,pos.y );

    //    Vec2 scrPosAgain = mMainCamera->GetWorldToScreen( pos );
    //    log( "screen again %f, %f", scrPosAgain.x, scrPosAgain.y );

    //    mMainCamera->ZoomAround( bg, 0);

    //    Rect res = mMainCamera->GetVisibleRect();
    //    log("---- %f %f %f %f", res.origin.x, res.origin.y, res.size.width, res.size.height );

}

void LevelsScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
        log("Exit game ...");
        CCDirector::Director::getInstance()->end();
    }
    mKeyDown[keyCode] = false;
//    log("key %i false", keyCode);
}
void LevelsScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    mKeyDown[keyCode] = true;
//    log("key %i true", keyCode);
}

void LevelsScene::ProcessKeyDown()
{
    if( mKeyDown[EventKeyboard::KeyCode::KEY_UP_ARROW] )
    {
        mMainCamera->setPosition3D( mMainCamera->getPosition3D() +
                                    Vec3(0,20,0));
//        mMainCamera->get
    }
    if( mKeyDown[EventKeyboard::KeyCode::KEY_DOWN_ARROW] )
    {
        mMainCamera->setPosition3D( mMainCamera->getPosition3D() +
                                    Vec3(0,-20,0));
    }
}
