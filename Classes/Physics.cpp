#include "Physics.h"

Physics::Physics()
{
    b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
    mWorld = new b2World( gravity );
    mWorld->SetAllowSleeping( false );
}

b2World* Physics::GetPhysicalWorld()
{

    return mWorld;
}

void Physics::DeleteAllBodies()
{
    b2Body* body = mWorld->GetBodyList();
    b2Body* next;
    while( body != nullptr )
    {
        next = body->GetNext();
        mWorld->DestroyBody( body );
        body = next;
    }
}

