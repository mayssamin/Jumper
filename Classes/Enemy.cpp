#include "Enemy.h"

Enemy::Enemy()
{

}

void Enemy::SetBits()
{

    setTag(OT_ENEMY);

    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();
    filter.categoryBits = OT_ENEMY;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
    AddCollidable( OT_PLAYER );
}

Enemy* Enemy::create()
{
    Enemy* enemy = new Enemy( );

    if ( enemy->initWithFile( "enemy.ini" ) )
    {
        enemy->SetBits();
        enemy->autorelease();

        return enemy;
    }

    CC_SAFE_DELETE( enemy );
    return NULL;
}

void Enemy::SetIsDangerous( bool is_dangerous)
{
    mIsDangerous = is_dangerous;
}

bool Enemy::GetIsDangerous()
{
    return mIsDangerous;
}
