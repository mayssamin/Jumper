#ifndef _PLAYER_H
#define _PLAYER_H

#include "cocos2d.h"
#include "GameObject.h"
#include "Platform.h"
#include "BubbleFuel.h"
#include "Enemy.h"

using namespace cocos2d;

enum HealthState
{
    HS_NORMAL = 1,
    HS_SMOTHERING = 2,
    HS_DIED = 100
};

enum JumpResult
{
    JS_OK,
    JS_GO_UP,
    JS_FALL,
    JS_STICK
};

class Player: public GameObject
{
private:
    bool mIsStarted;
    void Update(float dt);
    JumpResult CanJump(Platform* platform);
    void AddVerticalVelocity(Platform *platform);
    void HandleFuel(BubbleFuel *fuel);
    float mGasAmount = 10;
    float mGasCapacity = 20;
    float mRadius = 1;
    Platform* mLastExitedPlatform = nullptr;
    std::vector<GameObject*> mContacts, mFinishedContacts;
    float mMinimumHeightToJump;
    bool mIsSticked = false;
    float mShootPower;
    float mWaveSquaredRadius;
    void GenerateDefensiveWave();
    float mMinimumGasToLive;
    float mGasConsumeSpeed;
    float mLastTimeGasConsumed;
    float mSmotheringGasValue;
    HealthState mHealthState ;
    void Jump();
public:
    vector<Enemy*> mEnemies;
    Player();
    virtual void SetBits();
    void AddHorizontalVelocity( float value );
    static Player* create( /*std::string info_path*/ );
    void CollisionWith( GameObject* object );
    void CollisionFinishedWith( GameObject* object );
    ~Player();
    float GetGasAmount() const;
    void SetGasAmount(float gasAmount);
    void ResizeBall();
    bool IsAlive();

    void ProcessContactStartList();
    void ProcessContactFinishedList();
    void Dragged( Vec2 dragDirection);
    void DoubleTouch();


    float GetGasConsumeSpeed() const;
    void SetGasConsumeSpeed(float gasConsumeSpeed);
    void StartJumping();
    bool GetIsStarted();
};

#endif //_PLAYER_H
