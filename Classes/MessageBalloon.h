#ifndef _MESSAGEBALLOON_H
#define _MESSAGEBALLOON_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class MessageBalloon: public GameObject
{
private:
    Sprite* mMessageTail = nullptr;
    Label* mText;
    int mFontSize;
    string mFontFamily;
public:
    MessageBalloon();
    ~MessageBalloon(){}
    virtual void SetBits();
    static MessageBalloon* create(  );
    void SetText( string text );
};

#endif //_MESSAGEBALLOON_H
