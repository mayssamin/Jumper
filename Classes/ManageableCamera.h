#ifndef _MANAGEABLECAMERA_H
#define _MANAGEABLECAMERA_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

enum VerticalAlignment
{
    VA_NONE = 0x00,
    VA_MIDDLE = 0x01,
    VA_INNER_UP = 0x02,
    VA_INNER_BOTTOM = 0x04,
    VA_OUTER_UP = 0x08,
    VA_OUTER_BOTTOM = 0x0F
};

enum HorizontalAlignment
{
    HA_NONE = 0x00,
    HA_MIDDLE = 0x01,
    HA_INNER_LEFT = 0x02,
    HA_INNER_RIGHT = 0x04,
    HA_OUTER_LEFT = 0x08,
    HA_OUTER_RIGHT = 0x0F
};

class ManageableCamera : public Camera
{
private:
    Vec3 mTargetPosition;
public:
    // override set position to recalculate visibile rectangle ?
    ManageableCamera();
    ~ManageableCamera(){}
    void ZoomAround(GameObject *subject, float margin_radius);
    Rect GetVisibleRect( float z = 0 );
//    float ZoomToObjectsBounds( vector<GameObject*> objects);
    Vec3 GetScreenToWorld(Vec2 location , float z = 0 );
    Vec2 GetWorldToScreen(Vec3 worldPosition );
    static ManageableCamera* create( float fieldOfView, float aspectRatio, float nearPlane, float farPlane );
    void AlignWithObject(GameObject *subject,
                          VerticalAlignment vert_align,
                          HorizontalAlignment hor_align );
    virtual void lookAt(const Vec3& target, const Vec3& up = Vec3::UNIT_Y);
};

#endif //_MANAGEABLECAMERA_H
