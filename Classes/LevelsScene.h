#ifndef _LEVELSSCENE_H
#define _LEVELSSCENE_H

#include "cocos2d.h"
#include "ManageableCamera.h"
#include "Level.h"
#include "PlatformManager.h"
#include "GameLayer.h"

using namespace cocos2d;

class LevelsScene  : public GameLayer
{
public:
    static Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    CREATE_FUNC(LevelsScene);
    void update(float dt);

    virtual void onEnterTransitionDidFinish();
private:
    EventListenerKeyboard* mListenerKeyboard;
    map<EventKeyboard::KeyCode,bool> mKeyDown;
    Sprite* mLevels;
    void InitializeObjects();
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);

    PlatformManager* mPlatformManager;
    void ProcessKeyDown();
};

#endif //_LEVELSSCENE_H
