#ifndef _FISH_H
#define _FISH_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class Fish: public GameObject
{
private:
    bool mIsExposed = false;
    bool mIsStartedSwiming = false;
    bool mResting = false;
    float mRestRemainTime = 0;
    float mSwimSpeedX = 0;
    void Update(float dt);
public:
    Fish();
    ~Fish(){}
    virtual void SetBits();
    static Fish* create( );
    void Start();
};

#endif //_FISH_H
