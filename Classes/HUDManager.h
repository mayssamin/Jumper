#ifndef _HUDMANAGER_H
#define _HUDMANAGER_H

#include "cocos2d.h"

using namespace cocos2d;

class HUDManager
{
    Size mVisibleSize;
    Vec2 mOrigin;
public:
    HUDManager();
    ~HUDManager(){}
    void Initialize();
};

#endif //_HUDMANAGER_H
