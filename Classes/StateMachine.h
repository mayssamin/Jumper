#ifndef _STATEMACHINE_H
#define _STATEMACHINE_H

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

class State
{
protected:
    string mName;
    uint mTag;
    map<uint, State*> mNextStateMap;

public:
//    virtual void DoTransition( uint event_tag ) = 0;
    virtual void OnEnter( State* previous_state ) = 0;
    virtual void OnExit( State* next_state ) = 0;
};

class StateMachine
{
private:
    map<uint, State*> mStates;
    vector<uint> mEventQueue;
    State* mCurrentState;
public:
    StateMachine();
    ~StateMachine(){}
    void Initialize();
    void Update();
};

#endif //_STATEMACHINE_H
