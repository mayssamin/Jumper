#include "BubbleFuel.h"

BubbleFuel::BubbleFuel()
{

}

void BubbleFuel::SetBits()
{
    setTag(OT_FUEL);

    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();
    filter.categoryBits = OT_FUEL;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
    AddCollidable( OT_PLAYER );
}

BubbleFuel* BubbleFuel::create(/*std::string info_path*/)
{
    BubbleFuel* fuel = new BubbleFuel( );

    if ( fuel->initWithFile( "bubble_fuel.ini" ) )
    {
        fuel->SetBits();
        fuel->autorelease();

        return fuel;
    }

    CC_SAFE_DELETE( fuel );
    return NULL;
}
