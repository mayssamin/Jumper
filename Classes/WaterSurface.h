#ifndef _WATERSURFACE_H
#define _WATERSURFACE_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class WaterSurface : public GameObject
{
private:
    int mTilesTypeNumber = 1;
    string mTilesNamePrefix;
    string mTilesNamePostfix;
    void CreateTilesAndUpdateSize();
public:

    WaterSurface();
    virtual void SetBits();
    static WaterSurface* create();
    ~WaterSurface(){}
};

#endif //_WATERSURFACE_H
