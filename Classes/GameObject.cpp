#include "GameObject.h"
#include "GameScene.h"
#include <sstream>
#include <iostream>
//#include <boost/algorithm/string.hpp>
//using namespace boost::algorithm;
USING_NS_CC;
GameObject::GameObject(  )
{

}
//GameObject::~GameObject()
//{

//}


bool GameObject::initWithFile( std::string info_path )
{

    bool result = true;
    //std::ifstream infile(  );
    string lines = FileUtils::getInstance()->getStringFromFile(
                FileUtils::getInstance()->fullPathForFilename(info_path));
    //    CSimpleIniA ini(false, false, false);

    mProperties.SetMultiKey( );
    SI_Error rc = mProperties.LoadData( lines.c_str(), lines.size() );
    if (rc < 0)
    {
        log("error in gameobject data file (%s) !", info_path.c_str() );
        result = false;
    }
    else
    {
        string is_simple = GetPropertyFirstValue("general",
                                                 "is_simple");
        if( is_simple == "true" || is_simple == "yes" || is_simple == "y")
        {
            mIsSimple = true;
            InitSimple();
        }
        else
        {
            mIsSimple = false;
            InitComplicated();
        }
        InitializePhysics();
    }
    return result;
}

GameObject* GameObject::create( string info_path )
{
    GameObject* object = new GameObject( );

    if ( object->initWithFile( info_path ) )
    {
        object->autorelease();
        string name = object->GetPropertyFirstValue("general",
                                                 "name");
        if( name != "" )
        {
            object->setName( name );
        }

        return object;
    }

    CC_SAFE_DELETE( object );
    return NULL;
}

void GameObject::setPosition(float x, float y)
{
    Node::setPosition( x, y );
    if( mPhysicalBody != nullptr )
    {
        mPhysicalBody->SetTransform( b2Vec2( x/Setting::GetInstance().SCALE_RATIO,
                                             y/Setting::GetInstance().SCALE_RATIO),
                                     mPhysicalBody->GetAngle() );
    }
    ///@TODO: Move code below to Ball.cpp
    if( mMaxHeightTraveled < y )
    {
        mMaxHeightTraveled = y;
    }
}

void GameObject::setRotationWithPhysics( float rotation )
{
    Node::setRotation( rotation );

    if( mPhysicalBody != nullptr )
    {
        mPhysicalBody->SetTransform( mPhysicalBody->GetPosition(),
                                     -1 * CC_DEGREES_TO_RADIANS( rotation ) );
    }
}

b2Body* GameObject::GetPhysicalBody()
{
    return mPhysicalBody;
}

void GameObject::ComputeAABB( b2AABB& aabb)
{
    aabb.lowerBound = b2Vec2(FLT_MAX,FLT_MAX);
    aabb.upperBound = b2Vec2(-FLT_MAX,-FLT_MAX);
    b2Fixture* fixture = mPhysicalBody->GetFixtureList();

    while (fixture != nullptr)
    {
        int childCount = fixture->GetShape()->GetChildCount();
        for (int child = 0; child < childCount; ++child)
        {
            aabb.Combine(aabb, fixture->GetAABB( child ));
        }
        fixture = fixture->GetNext();
    }
}

void GameObject::AddCollidable(ObjectType objectType )
{
    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();
    filter.maskBits = filter.maskBits | objectType;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
}

void GameObject::RemoveCollidable(ObjectType objectType )
{
    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();
    filter.maskBits = filter.maskBits & ~objectType;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
}

bool GameObject::InitializeStaticSprite()
{
    bool result = true;
    string pref = GetPropertyFirstValue("sprite", "name_prefix");
    string post = GetPropertyFirstValue("sprite", "name_postfix");
    string default_state = GetPropertyFirstValue("sprite", "default_state");

    if( pref != "" && post != ""  )
    {
        string name = pref+default_state+post;
//        log("################ %s", name.c_str() );
        mSprite = Sprite::createWithSpriteFrameName( name );/*create();
            mSprite->initWithFile( property );*/
        this->addChild( mSprite );
    }
    else
    {
        result = false;
    }
    return result;
}

bool GameObject::InitializeAnimatedSprite()
{
    bool result = true;
    string pref = GetPropertyFirstValue("sprite", "name_prefix");
    string post = GetPropertyFirstValue("sprite", "name_postfix");
    string default_frame = GetPropertyFirstValue("sprite", "default_frame");

    if( post != "" && default_frame != "" )
    {
        mDefaultFrameName = pref+default_frame+post;
        mSprite = Sprite::createWithSpriteFrameName( mDefaultFrameName );/*create();
            mSprite->initWithFile( property );*/
        this->addChild( mSprite );
    }
    else
    {
        result = false;
    }
    return result;
}

bool GameObject::InitializePhysics()
{
    bool result = true;
    //Create Collider

    AddPhysicalBody( GetPropertyFirstValue("physics", "collider"),
                     GetPropertyFirstValue("physics", "body_type"),
                     GetPropertyFirstValue("physics", "is_sensor"),
                     GetPropertyFirstValue("physics", "initial_velocity"),
                     GetPropertyFirstValue( "physics", "material") );
    return result;
}

Vec2 GameObject::GetBoundingSize()
{
    return mBoundingSize;
}

string GameObject::GetPropertyFirstValue( string section, string key )
{
    string property = "";
    //    log( "finding value of %s in %s", key.c_str(), section.c_str());
    CSimpleIni::TNamesDepend values;
    if (mProperties.GetAllValues(section.c_str(), key.c_str(), values))
    {
        CSimpleIni::TNamesDepend::const_iterator i = values.begin();

        property = i->pItem;
        //            log("value found %s", i->pItem);

    }
    //    else
    //    {
    //        log("value not found");
    //    }
    return property;
}

vector<string> GameObject::GetPropertyValues( string section, string key )
{
    vector<string> valuesString;
    //    log( "finding values of %s in %s", key.c_str(), section.c_str());
    CSimpleIni::TNamesDepend values;
    if (mProperties.GetAllValues(section.c_str(), key.c_str(), values))
    {
        CSimpleIni::TNamesDepend::const_iterator i = values.begin();
        for (; i != values.end(); ++i)
        {
            valuesString.push_back( i->pItem );
            //            log("value found %s", i->pItem);
        }
    }
    //    else
    //    {
    //        log("value not found");
    //    }
    return valuesString;
}

void GameObject::CreateAnimations()
{

//    Vector<SpriteFrame *> frames;
//    SpriteFrameCache *frameCache = SpriteFrameCache::getInstance();
    vector<string> animations = GetPropertyValues( "animations", "animation");
    for( vector<string>::iterator animit = animations.begin();
         animit != animations.end(); animit++)
    {
        CreateAnimation( *animit );

    }
}

void GameObject::CreateAnimation(string anim)
{
//    log("anim: %s", anim.c_str() );
    string pref = GetPropertyFirstValue("sprite", "name_prefix");
    string post = GetPropertyFirstValue("sprite", "name_postfix");

    int nameEndIndex = anim.find("(");
    if( nameEndIndex > -1 )
    {
        string animName = anim.substr( 0, nameEndIndex);
        trim(animName);
//        log(" anim name %s", animName.c_str() );
        int bracStIdx = anim.find("[");
        int bracEndIdx = anim.find("]");
        if( bracStIdx > -1 && bracEndIdx > -1)
        {
            string animFrames = anim.substr( bracStIdx + 1, bracEndIdx - bracStIdx - 1 );
            trim(animFrames);
//            log(" anim frames %s", animFrames.c_str() );
            int delayIdx = anim.find_last_of(",");
            int delayEndIdx = anim.find(")");
            if( delayIdx > -1 && delayEndIdx > -1 )
            {
                string delay = anim.substr( delayIdx+1, delayEndIdx - delayIdx -1 );
                trim( delay );
//                log(" anim delay %s", delay.c_str() );


                vector<string> v{explode(animFrames, ',')};
                Vector<SpriteFrame *> frames;
                SpriteFrameCache *frameCache = SpriteFrameCache::getInstance();
                for(auto n:v)
                {
                    trim(n);
//                    cout << pref << n << post << endl;
                    SpriteFrame *frame = frameCache->getSpriteFrameByName( pref + n + post );
                    frames.pushBack(frame);
                }
                Animation *animation = Animation::createWithSpriteFrames(
                            frames, atof(delay.c_str()) );
                Animate* anim = new Animate();
                anim->initWithAnimation( animation );
                mAnimations[ animName ] = anim;

                //                animated->startWithTarget( mSprite );

            }
        }
    }
    //        char file[100] = {0};

    //        for (int i = 0; i < 1; i++)
    //        {
    //            sprintf(file, "ninja-running-e%04d.png", i);
    //            SpriteFrame *frame = frameCache->getSpriteFrameByName(file);
    //            frames.pushBack(frame);
    //        }

    //            Animation *animation = Animation::createWithSpriteFrames(frames, 0.05);
    //    Animate *animate = Animate::create(animation);
}
Sprite *GameObject::GetSprite() const
{
    return mSprite;
}

void GameObject::SetSprite(Sprite *sprite)
{
    mSprite = sprite;
}


void GameObject::PlayAnimation( string animation_name, bool repeat )
{
    if( mIsSimple )
    {
        Animate* anim = mAnimations[animation_name];
        if( anim != nullptr)
        {
            mSprite->stopAction( anim );
            if(repeat)
            {
                mSprite->runAction( RepeatForever::create( anim ) );
            }
            else
            {
                mSprite->runAction( anim  );
            }
        }
    }
    else
    {
        for( vector<GameObject*>::iterator objit = mChildrenObjects.begin();
             objit != mChildrenObjects.end(); objit++ )
        {
            ( (GameObject*) *objit)->PlayAnimation( animation_name, repeat );
        }
    }
}

void GameObject::StopAnimation( string animation_name, bool set_to_default)
{
    if( mIsSimple )
    {
        Action* jump = mAnimations[animation_name];
        if( jump != nullptr )
        {
            mSprite->stopAction( jump );
            if( set_to_default )
            {
                mSprite->setSpriteFrame( mDefaultFrameName );
            }
        }
    }
    else
    {
        for( vector<GameObject*>::iterator objit = mChildrenObjects.begin();
             objit != mChildrenObjects.end(); objit++ )
        {
            ( (GameObject*) *objit)->StopAnimation( animation_name );
        }
    }
}

bool GameObject::InitSimple()
{
    bool result = true;
    string property = GetPropertyFirstValue("sprite",
                                            "is_animated");
    if( property == "true" || property == "yes" || property == "y")
    {
        result = result && InitializeAnimatedSprite();
        CreateAnimations();
    }
    else
    {
        result = result && InitializeStaticSprite();

    }

    property = GetPropertyFirstValue( "sprite", "scale");
    if( property != "" )
    {
        int index = property.find(",");
        if( index > -1 )
        {
            std::string first = property.substr( 0, index);
            trim(first);
            std::string second = property.substr( index+1 );
            trim(second);
            //log("force %f %f", stof(first), stof(second));

            mSprite->setScale( atof(first.c_str()), atof(second.c_str()) );

        }
    }
    mBoundingSize.x = mSprite->getBoundingBox().size.width;
    mBoundingSize.y = mSprite->getBoundingBox().size.height;
    return result;
}

void GameObject::InitComplicated()
{
    vector<string> children = GetPropertyValues( "children", "child");
    for( vector<string>::iterator childit = children.begin();
         childit != children.end(); childit++)
    {
        auto go = GameObject::create( *childit );

        Vec2 childBounding = go->GetBoundingSize();
        if( childBounding.x > mBoundingSize.x )
        {
            mBoundingSize.x = childBounding.x;
        }
        if( childBounding.y > mBoundingSize.y )
        {
            mBoundingSize.y = childBounding.y;
        }
        addChild( go );
//        go->initWithFile( *childit );
        mChildrenObjects.push_back( go );

    }
}

void GameObject::AddPhysicalBody( string collider_type, string body_type, string is_sensor,
                                  string initial_velocity, string material)
{
    string property = collider_type;
    if( property != "" )
    {
        if( property == "auto_circle" )
        {
            mShape = new b2CircleShape();

            mShape->m_radius = ( mBoundingSize.x/2 ) /
                    Setting::GetInstance().SCALE_RATIO;

            b2FixtureDef fixture;
            fixture.shape = mShape;
            fixture.filter.maskBits = OT_NONE;
            //            fixture.density = .1;
            //            fixture.friction = 1;
            //            fixture.restitution = 0;
            mBodyDef = new b2BodyDef();
            mBodyDef->type = b2_staticBody;
            mBodyDef->userData = this;
            mPhysicalBody = Physics::GetInstance().GetPhysicalWorld()->CreateBody(mBodyDef);
            mPhysicalBody->CreateFixture(&fixture);

        }
        else if( property == "auto_box" )
        {
            mShape = new b2PolygonShape();
            ( (b2PolygonShape*)mShape)->SetAsBox( mBoundingSize.x/
                                                  Setting::GetInstance().SCALE_RATIO/2,
                                                  mBoundingSize.y/
                                                  Setting::GetInstance().SCALE_RATIO/2 );
            b2FixtureDef fixture;
            fixture.shape = mShape;
            //            fixture.density = .1;
            //            fixture.friction = 1;
            //            fixture.restitution = 0;
            mBodyDef = new b2BodyDef();
            mBodyDef->type = b2_staticBody;
            mBodyDef->userData = this;
            mPhysicalBody = Physics::GetInstance().GetPhysicalWorld()->CreateBody(mBodyDef);
            mPhysicalBody->CreateFixture(&fixture);
        }
    }
    property = body_type;
    if( property != "" )
    {
        if( property == "dynamic" )
        {
            if(mPhysicalBody != nullptr)
            {
                mPhysicalBody->SetType( b2_dynamicBody );
            }
        }
        else if( property == "kinematic" )
        {
            if(mPhysicalBody != nullptr)
            {
                mPhysicalBody->SetType( b2_kinematicBody );
            }
        }
    }
    property = is_sensor;
    if( property != "" )
    {
        if( property == "yes" )
        {
            if(mPhysicalBody != nullptr)
            {
                mPhysicalBody->GetFixtureList()->SetSensor( true );
            }
        }
    }
    property = initial_velocity;
    if( property != "" )
    {
        int index = property.find(",");
        if( index > -1 )
        {
            std::string first = property.substr( 0, index);
            trim(first);
            std::string second = property.substr( index+1 );
            trim(second);
            //log("force %f %f", stof(first), stof(second));

            if( mPhysicalBody != nullptr)
            {
                mPhysicalBody->SetLinearVelocity(b2Vec2( atof(first.c_str())/Setting::GetInstance().SCALE_RATIO,
                                                         atof(second.c_str())/Setting::GetInstance().SCALE_RATIO));
            }

        }
    }
    property = material;
    if( property != "" )
    {
        int index = property.find(",");
        if( index > -1 )
        {
            std::string first = property.substr( 0, index);
            trim(first);
            int index2 = property.find(",",index+1 );
            if( index2 > -1 )
            {
                std::string second = property.substr( index+1, index2 - index - 1 );
                trim(second);
                std::string third = property.substr( index2+1 );
                trim(third);
                //                log(first.c_str());
                //                log(second.c_str());
                //                log(third.c_str());
                if(mPhysicalBody != nullptr)
                {
                    //                    log("Material %f %f %f", atof(first.c_str()) , atof(second.c_str())  , atof(third.c_str()) );
                    mPhysicalBody->GetFixtureList()->SetDensity( atof(first.c_str()) );
                    mPhysicalBody->GetFixtureList()->SetRestitution( atof(second.c_str() ) );
                    mPhysicalBody->GetFixtureList()->SetFriction( atof(third.c_str()) );
                    mPhysicalBody->ResetMassData();
                }
            }
        }
    }
}
