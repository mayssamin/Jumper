#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "setting.hpp"
#include "Physics.h"
#include "Player.h"
#include "Platform.h"
#include "BubbleFuel.h"
#include "Enemy.h"
#include "ContactListener.h"
#include "GLES-Render.h"
#include "InfiniteParallaxNode.h"
#include "PlatformManager.h"
#include "HUDManager.h"
#include "StateMachine.h"
#include "ManageableCamera.h"
#include "GameLayer.h"

USING_NS_CC;

class GameScreen : public GameLayer
{

public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual void onEnterTransitionDidFinish();
    // implement the "static create()" method manually
    CREATE_FUNC(GameScreen);

    bool onTouchBegan(Touch* touch, Event* event);
    void onTouchMoved(Touch* touch, Event* event);
    void onTouchEnded(Touch* touch, Event* event);
    bool onContactBegin(PhysicsContact& contact);
    void update(float dt);
    void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);

//    ManageableCamera* GetMainCamera() { return mMainCamera; }
    Size GetVisibleSize() { return mVisibleSize; }

    void AddToHUDLayer( Node* node);
    void RemoveFromMainLayer( Node* node);
private:
    Vec3 mBallInitialPosition;
    bool mObjectsCreated = false;
    Size mVisibleSize;
    Vec2 mOrigin;
    Player* mBall;
    Vec3 mAcceleration;
    Vec2 mDragOffsetStart, mDragOffsetEnd;
    clock_t mLastTouchEndTime;
    float mTimeBetweenDoubleTouch;//Seconds
    EventListenerKeyboard* mListenerKeyboard;
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
    void onAcceleration(Acceleration *acc, Event *event);
    MyContactListener *mContactListener;
    GLESDebugDraw *mDebugDraw;
    bool mDrawDebug = true;
    void GenerateFuel( int fuelCount );
    void GenerateEnemy( int enemyCount );
    void Restart();
    Camera* mHUDCamera;
//    ManageableCamera* mMainCamera;

    void ProcessContacts();
    InfiniteParallaxNode* mParallaxBackground;
    InfiniteParallaxNode* mParallaxForground;
    Sprite* mBackground;
    PlatformManager* mPlatformManager;
    HUDManager* mHUDManager;
    StateMachine* mStateManager;
};

#endif // __GAME_SCENE_H__
