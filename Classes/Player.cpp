﻿#include "Player.h"
#include "Physics.h"
#include "GameScene.h"
Player::Player():GameObject( )
{
    mMinimumGasToLive = 40;
    mMinimumHeightToJump = 0;
    mShootPower = 200;
    mWaveSquaredRadius = 400*400;
    mLastTimeGasConsumed = 0;
    mSmotheringGasValue = 42;
    mHealthState = HS_NORMAL;
    this->schedule( schedule_selector( Player::Update ) , .025 );
    mIsStarted = false;
}

Player::~Player()
{
    unscheduleAllCallbacks();
}
void Player::Update( float dt )
{
    mLastTimeGasConsumed += dt;
    if( mLastTimeGasConsumed > 1 )
    {
        SetGasAmount( GetGasAmount() - mGasConsumeSpeed );
        mLastTimeGasConsumed = 0;
    }
}

void Player::SetBits()
{
    setTag(OT_PLAYER);

    b2Filter filter = mPhysicalBody->GetFixtureList()->GetFilterData();

    filter.categoryBits = OT_PLAYER;
    mPhysicalBody->GetFixtureList()->SetFilterData( filter );
    AddCollidable( OT_FUEL );
    AddCollidable( OT_ENEMY );
    AddCollidable( OT_PLATFORM );
    AddCollidable( OT_GROUND );
}

Player* Player::create()
{
    Player* ball = new Player( );
    if ( ball->initWithFile( "player.ini" ) )
    {
        ball->SetBits();
        std::string property = ball->GetPropertyFirstValue("special", "gas_capacity");
        if( property != "")
        {
            trim(property);
            ball->mGasCapacity = atof( property.c_str() );
        }
        property = ball->GetPropertyFirstValue("special", "gas_amount");
        if( property != "")
        {
            trim(property);
            ball->SetGasAmount( atof( property.c_str() ) );
        }
        property = ball->GetPropertyFirstValue("special", "gas_amount");
        if( property != "")
        {
            trim(property);
            ball->SetGasAmount( atof( property.c_str() ) );
        }
        property = ball->GetPropertyFirstValue("special", "min_gas_to_live");
        if( property != "")
        {
            trim(property);
            ball->mMinimumGasToLive = atof( property.c_str() );
        }
        property = ball->GetPropertyFirstValue("special", "gas_consume_speed");
        if( property != "")
        {
            trim(property);
            ball->mGasConsumeSpeed = atof( property.c_str() );
        }
        property = ball->GetPropertyFirstValue("special", "smothering_gas_value");
        if( property != "")
        {
            trim(property);
            ball->mSmotheringGasValue = atof( property.c_str() );
        }

        ball->autorelease();

        return ball;
    }

    CC_SAFE_DELETE( ball );
    return NULL;
}


void Player::AddHorizontalVelocity( float value )
{
    //    log("horizontal vel %f", mPhysicalBody->GetLinearVelocity().x);
    Vec2 vel( value*30, 0);

    mPhysicalBody->SetLinearVelocity( b2Vec2( vel.x,
                                              mPhysicalBody->GetLinearVelocity().y) );


}

void Player::CollisionWith( GameObject* object )
{
    mContacts.push_back( object );
}

void Player::CollisionFinishedWith( GameObject* object )
{
    mFinishedContacts.push_back( object );
}
void Player::AddVerticalVelocity( Platform* platform)
{
    if( mPhysicalBody != nullptr )
    {
        JumpResult js = CanJump(platform);
        if( js == JS_OK)
        {
            Jump();
            platform->PlayerJumpedOn();
        }
        else if( js == JS_STICK )
        {
            mPhysicalBody->SetType( b2_staticBody );
            mPhysicalBody->SetLinearVelocity( b2Vec2( 0, 0) );
            mIsSticked = true;
            StopAnimation( "jump" );
        }
        else if( js == JS_FALL )
        {
            platform->PlayerJumpedOn();
        }

    }
}

void Player::HandleFuel(BubbleFuel* fuel)
{
    SetGasAmount( GetGasAmount() + 2 );
    GameScreen* mainLayer = (GameScreen*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    mainLayer->mScheduledRemove.push_back( fuel );
    //    fuel->removeFromParent();
}
float Player::GetGasAmount() const
{
    return mGasAmount;
}

void Player::SetGasAmount(float gasAmount)
{
    if( gasAmount > mGasCapacity )
    {
        mGasAmount = mGasCapacity;
    }
    else
    {
        mGasAmount = gasAmount;
    }
    mRadius = mGasAmount;
    //    log("gas amount %f", gasAmount);
    ResizeBall();
}


JumpResult Player::CanJump(Platform* platform)
{
    JumpResult result = JS_FALL;

    if( platform->IsStrong() &&
            GetPhysicalBody()->GetLinearVelocity().y <= 0)
    {
        result = JS_OK;
    }
    else if( platform->IsStrong() )
    {
        result = JS_GO_UP;
    }
    return result;
}

void Player::ResizeBall( )
{
    auto bubble = (GameObject*) getChildByName( "bubble" );
    bubble->setScale( mRadius/GetBoundingSize().x );

}

bool Player::IsAlive()
{
    bool result = false;

    if( mHealthState == HS_NORMAL && mGasAmount < mSmotheringGasValue &&
            mGasAmount >= mMinimumGasToLive )
    {
        GameObject* character = (GameObject*) getChildByName( "character" );
        string pref = character->GetPropertyFirstValue("sprite", "name_prefix");
        string post = character->GetPropertyFirstValue("sprite", "name_postfix");
        string state = "smothering";

        string name = pref+state+post;
        character->GetSprite()->setSpriteFrame( name );
        mHealthState = HS_SMOTHERING;
    }
    else if( mHealthState == HS_SMOTHERING && mGasAmount >= mSmotheringGasValue )
    {
        GameObject* character = (GameObject*) getChildByName( "character" );
        string pref = character->GetPropertyFirstValue("sprite", "name_prefix");
        string post = character->GetPropertyFirstValue("sprite", "name_postfix");
        string state = "normal";

        string name = pref+state+post;
        character->GetSprite()->setSpriteFrame( name );
        mHealthState = HS_NORMAL;
    }
    if( mGasAmount >= mMinimumGasToLive )
    {
        result = true;
    }
    else
    {
        mHealthState = HS_DIED;
        log("state %i gas %f", mHealthState, mGasAmount);
    }
    return result;
}

void Player::ProcessContactStartList()
{
    for( vector<GameObject*>::iterator objit = mContacts.begin();
         objit != mContacts.end(); objit++)
    {
        GameObject* object = *objit;
        if( object->getTag() == OT_PLATFORM )
        {
            AddVerticalVelocity( (Platform*) object );
        }
        else if( object->getTag() == OT_FUEL )
        {
            HandleFuel( (BubbleFuel*) object );
        }
        else if( object->getTag() == OT_ENEMY )
        {
            if( ((Enemy*) object)->GetIsDangerous() )
            {
                mGasAmount = 0;
            }
        }

    }
    mContacts.clear();
}

void Player::ProcessContactFinishedList()
{
    for( vector<GameObject*>::iterator objit = mFinishedContacts.begin();
         objit != mFinishedContacts.end(); objit++)
    {

        GameObject* object = *objit;
        if(object && object->getTag() == OT_PLATFORM )
        {
            Platform* platform = (Platform*) object;
            if( getPosition().y >= platform->getPosition().y )
            {
                mLastExitedPlatform = platform;
            }
        }
    }
    mFinishedContacts.clear();
}

void Player::Dragged( Vec2 dragDirection)
{
    if( mIsSticked )
    {
        Vec2 velocity = -1 * dragDirection;
        velocity.normalize();
        velocity.scale( mShootPower );
        log( "drag %f %f",dragDirection.x, dragDirection.y );
        log( "velocity %f %f",velocity.x, velocity.y );
        mPhysicalBody->SetType( b2_dynamicBody );
        mPhysicalBody->SetLinearVelocity( b2Vec2( velocity.x, velocity.y ) );
        mIsSticked = false;


        //Reduce gas amount
        SetGasAmount( GetGasAmount() - 1.0f );
    }
}

void Player::DoubleTouch()
{
    //    log("Double touch ...");
    GenerateDefensiveWave();
}

void Player::GenerateDefensiveWave()
{
    if( (GetGasAmount() - 1) >= mMinimumGasToLive )
    {
        SetGasAmount( GetGasAmount() - 1 );
        auto _emitter = ParticleSystemQuad::create("explosion_particle.plist");
        //            _emitter->setTextureWithRect(Director::getInstance()->getTextureCache()->addImage("Images/particles.png"), Rect(0,0,32,32));
        this->getParent()->addChild(_emitter, 10);
        _emitter->setCameraMask( (unsigned short)CameraFlag::USER2 );
        _emitter->setPosition(getPosition());
        for( vector<Enemy*>::iterator enit = mEnemies.begin();
             enit != mEnemies.end();  )
        {
            Enemy* enemy = *enit;
            float distSquared = (enemy->getPosition() - getPosition()).lengthSquared();

            if( distSquared < mWaveSquaredRadius )
            {
                enemy->SetIsDangerous( false );
                GameScreen* mainLayer = (GameScreen*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
                mainLayer->mScheduledRemove.push_back( enemy );
                enit = mEnemies.erase( enit );
            }
            else
            {
                enit++;
            }
        }
    }
}
float Player::GetGasConsumeSpeed() const
{
    return mGasConsumeSpeed;
}

void Player::SetGasConsumeSpeed(float gasConsumeSpeed)
{
    mGasConsumeSpeed = gasConsumeSpeed;
}

bool Player::GetIsStarted()
{
    return mIsStarted;
}

void Player::StartJumping()
{
    mIsStarted = true;
    Jump();
}

void Player::Jump()
{
    mPhysicalBody->SetLinearVelocity( b2Vec2( 0, 20 ) );
    PlayAnimation( "jump");
     auto _emitter = ParticleSystemQuad::create("particle.plist");
    this->getParent()->addChild(_emitter, 10);
    _emitter->setCameraMask( (unsigned short)CameraFlag::USER2 );
    _emitter->setPosition(getPosition());

}
