#include "GameScene.h"
#include "utils.h"
#include "GameObject.h"
#include "MessageBalloon.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../cocos2d/cocos/platform/android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#endif

Scene* GameScreen::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create/*WithPhysics*/();
    //scene->getPhysicsWorld()->setGravity( Vect( 0, -10 ) );
    
    //scene->getPhysicsWorld()->setSpeed( 7 );
    // 'layer' is an autorelease object
    auto layer = GameScreen::create();
    layer->setName("main_layer");
    layer->setTag( 100 );
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if( !Layer::init() )
    {
        return false;
    }

    //    this->setCameraMask((unsigned short)CameraFlag::USER2, true);
    mMainLayer = Layer::create();
    mMainLayer->setCameraMask( (unsigned short)CameraFlag::USER2 );
    this->addChild( mMainLayer );
    
    mHUDLayer = Layer::create();
    mHUDLayer->setCameraMask( (unsigned short)CameraFlag::USER1 );
    this->addChild( mHUDLayer );
    
    
    //mIsAtStartup = true;
    //SET MOUSE LISTENER
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(GameScreen::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameScreen::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameScreen::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    //END MOUSE LISTENER
    
    mListenerKeyboard = EventListenerKeyboard::create();
    mListenerKeyboard->onKeyReleased = CC_CALLBACK_2(GameScreen::onKeyReleased, this);
    mListenerKeyboard->onKeyPressed = CC_CALLBACK_2(GameScreen::onKeyPressed, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mListenerKeyboard, this);
    
    
    mVisibleSize = Director::getInstance()->getVisibleSize();
    mOrigin = Director::getInstance()->getVisibleOrigin();
    log("origin %f %f", mOrigin.x, mOrigin.y);
    auto cacher = SpriteFrameCache::getInstance();
    cacher->addSpriteFramesWithFile("sprites.plist");
    // Create contact listener
    
    //----------------
    Device::setAccelerometerEnabled(true);
    auto acclistener = EventListenerAcceleration::create(CC_CALLBACK_2(GameScreen::onAcceleration, this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(acclistener , this);
    
    mAcceleration = Vec3( 0, 0, 0 );
    
    // Enable debug draw
    mDebugDraw = new GLESDebugDraw( Setting::GetInstance().SCALE_RATIO );
    Physics::GetInstance().GetPhysicalWorld()->SetDebugDraw( mDebugDraw );
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    mDebugDraw->SetFlags( flags );

    mMainCamera = ManageableCamera::create(60, (float)mVisibleSize.width/mVisibleSize.height,
                                            1.0, 1500);
    mMainCamera->setCameraFlag(CameraFlag::USER2);
    //the calling order matters, we should first call setPosition3D, then call lookAt.
    this->addChild(mMainCamera);

    //    mMainCamera = Camera::createPerspective(60, (float)mVisibleSize.width/mVisibleSize.height,
    //                                            1.0, 1500);
    //    mMainCamera->setCameraFlag(CameraFlag::USER2);
    //    //the calling order matters, we should first call setPosition3D, then call lookAt.
    //    mMainCamera->setPosition3D(mBall->getPosition3D() + Vec3(0,0,800));
    //    mMainCamera->lookAt(mBall->getPosition3D(), Vec3(0.0,1.0,0.0));
    //    this->addChild(mMainCamera);
    
    mHUDCamera = Camera::createPerspective(60, (float)mVisibleSize.width/mVisibleSize.height, 1.0, 1000);
    mHUDCamera->setCameraFlag(CameraFlag::USER1);
    //the calling order matters, we should first call setPosition3D, then call lookAt.
    mHUDCamera->setPosition3D( Vec3( 0, 0, 0) + Vec3(0,0,800));
    mHUDCamera->lookAt(Vec3( 0, 0, 0), Vec3(0.0,1.0,0.0));
    this->addChild(mHUDCamera);
    
    //    mMainCamera->setPosition3D(mBall->getPosition3D() + Vec3(0,0,1200));
    
    mContactListener = new MyContactListener();
    Physics::GetInstance().GetPhysicalWorld()->SetContactListener( mContactListener );
    

    scheduleUpdate();
    log("update registered");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    setKeepScreenOnJni( true );
#endif
    mDragOffsetStart.x = 0;
    mDragOffsetStart.y = 0;
    mDragOffsetEnd.x = 0;
    mDragOffsetEnd.y = 0;
    
    mLastTouchEndTime = 0;
    mTimeBetweenDoubleTouch = .2;
    mBallInitialPosition = Vec3(0,0,0);


    mPlatformManager = new PlatformManager();
    mHUDManager = new HUDManager();
    mStateManager = new StateMachine();

    //    GameObject* alaki = GameObject::create( "stem.ini" );
    //    alaki->setPosition( 0,0);
    //    Vec2 startPos( -mVisibleSize.width/2, -mVisibleSize.height/2 );
    //    Vec2 endPos( mVisibleSize.width/2, mVisibleSize.height/2 );
    //    Vec2 horiline = Vec2( 1000, startPos.y ) - startPos;
    //    float len = (endPos - startPos).length();
    //float deg = Vec2::angle( (endPos - startPos), horiline );
    //    log( "################### %f ", CC_RADIANS_TO_DEGREES(deg) );
    //    alaki->setScaleY( len/alaki->GetBoundingSize().y );
    //alaki->setRotation( CC_RADIANS_TO_DEGREES(deg) );
    //    AddToHUDLayer( alaki );



    return true;
}


void GameScreen::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{

    //    if(mDrawDebug)
    //    {
    //        glDisable(GL_TEXTURE_2D);
    //        glDisableClientState(GL_COLOR_ARRAY);
    //        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    //        Physics::GetInstance().GetPhysicalWorld()->DrawDebugData();
    //        glEnable(GL_TEXTURE_2D);
    //        glEnableClientState(GL_COLOR_ARRAY);
    //        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //    }
    //    else
    //    {
    Layer::draw(renderer, transform, flags);
    //    }
}

void GameScreen::onEnterTransitionDidFinish()
{
    if( !mObjectsCreated )
    {
        Restart();
        mHUDManager->Initialize();
        mStateManager->Initialize();
        mObjectsCreated = true;
    }
    
}

void GameScreen::update(float dt)
{
    mStateManager->Update();
    int positionIterations = 10;
    int velocityIterations = 10;
    
    
    
    
    b2World* world = Physics::GetInstance().GetPhysicalWorld();
    //    log("dt %f",dt);
    world->Step(dt*2, velocityIterations, positionIterations);
    
    for (b2Body *body = world->GetBodyList(); body != NULL; body = body->GetNext())
        if (body->GetUserData())
        {
            GameObject *object = (GameObject*) body->GetUserData();
            object->setPosition( body->GetPosition().x * Setting::GetInstance().SCALE_RATIO,
                                 body->GetPosition().y * Setting::GetInstance().SCALE_RATIO );
            object->setRotation(-1 * CC_RADIANS_TO_DEGREES(body->GetAngle()));
        }
    ProcessContacts();
    for( std::vector<Node*>::iterator goiter = mScheduledRemove.begin();
         goiter != mScheduledRemove.end(); )
    {
        Node* node = *goiter;
        if( node->getTag() > 0 && node->getTag() < 1000 )
        {
            GameObject* go = (GameObject*) *goiter;
            if( go->GetPhysicalBody() != nullptr )
            {
                go->GetPhysicalBody()->SetUserData( nullptr );
                Physics::GetInstance().GetPhysicalWorld()->DestroyBody( go->GetPhysicalBody() );
            }
            go->removeFromParentAndCleanup( true );
            goiter = mScheduledRemove.erase( goiter );
        }
        else
        {
            node->removeFromParentAndCleanup( true );
            goiter = mScheduledRemove.erase( goiter );
        }
    }
    world->ClearForces();
    world->DrawDebugData();
    if( (mBall->getPosition3D().y - mMainCamera->getPosition3D().y) >= 0 )
    {
        
        mMainCamera->setPosition3D( Vec3(mMainCamera->getPosition3D().x,
                                         mBall->getPosition3D().y, mMainCamera->getPosition3D().z));
        mMainCamera->lookAt(Vec3(mMainCamera->getPosition3D().x,
                                 mBall->getPosition3D().y, mBall->getPosition3D().z), Vec3(0.0,1.0,0.0));
        
        mParallaxBackground->setPositionY( mBall->getPositionY() );
        mParallaxForground->setPositionY( mBall->getPositionY() );
    }
    if( (mBall->getPosition3D().y - mMainCamera->getPosition3D().y) < -700 ||
            !mBall->IsAlive() )
    {
        Restart();

    }

    mPlatformManager->Update();
    //    Vec2 viewOffset = Vec2::ZERO,
    //            viewSize = Vec2(mVisibleSize.width, mVisibleSize.height);
    
    //    Vec2 windowSpacePos =  WorldToScreen( mMainCamera->getProjectionMatrix(),
    //                                          mMainCamera->getViewMatrix(),
    //                                          mAlaki->getPosition3D(), viewOffset, viewSize ) ;
    //    log("******* pos %f %f", windowSpacePos.x, windowSpacePos.y );
}


bool GameScreen::onTouchBegan(Touch* touch, Event* event)
{
    mDragOffsetStart.x = touch->getLocation().x;
    mDragOffsetStart.y = touch->getLocation().y;
    return true;
}

void GameScreen::onTouchMoved(Touch* touch, Event* event)
{
}

void GameScreen::onTouchEnded(Touch* touch, Event* event)
{
    if( mBall->GetIsStarted() )
    {

        float diff = float( clock () - mLastTouchEndTime ) /  CLOCKS_PER_SEC;
        //    mLastTouchEndTime = clock();
        //    log("clicked! %f", diff);
        if( diff <= mTimeBetweenDoubleTouch )
        {
            mBall->DoubleTouch();
            mLastTouchEndTime = 0;
        }
        else
        {
            mLastTouchEndTime = clock ();
        }
        mDragOffsetEnd.x = touch->getLocation().x;
        mDragOffsetEnd.y = touch->getLocation().y;
        Vec2 dragVector = mDragOffsetEnd - mDragOffsetStart;
        //    if( dragVector.x > 0 || dragVector.y > 0 )
        //    {
        mBall->Dragged( dragVector );
    }
    else
    {
        mBall->StartJumping();
    }
    //    }
}

void GameScreen::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
        log("Exit game ...");
        CCDirector::Director::getInstance()->end();
    }
    else if(keyCode == EventKeyboard::KeyCode::KEY_0)
    {
        mDrawDebug = !mDrawDebug;
        
    }
}
void GameScreen::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    if(keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW )
    {
        mBall->AddHorizontalVelocity( .1 );
    }
    else if(keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW )
    {
        mBall->AddHorizontalVelocity( -.1 );
    }
    
}

void GameScreen::onAcceleration(Acceleration *acc, cocos2d::Event *event)
{
    //log("diff %f" , acc->x - mAcceleration.x );
    mBall->AddHorizontalVelocity( mAcceleration.x  );
    mAcceleration = Vec3(acc->x, acc->y, acc->z);
}

void GameScreen::GenerateFuel(int fuelCount)
{
    float lastHeight = 0;
    float height = cocos2d::random( mBallInitialPosition.y + 100.0f,
                                    mBallInitialPosition.y + 200.0f);
    float width = cocos2d::random( mBallInitialPosition.x - 100,
                                   mBallInitialPosition.x + 250.0f);
    for( int platformNumber = 0; platformNumber < fuelCount; platformNumber++ )
    {
        auto objplat = BubbleFuel::create( );
        mMainLayer->addChild( objplat );
        objplat->setCameraMask( (unsigned short)CameraFlag::USER2 );
        objplat->setPosition( width, lastHeight + height );
        objplat->setLocalZOrder( 1 );
        lastHeight += height;
        height = cocos2d::random( 200.0f, 400.0f);
        width = cocos2d::random( mBallInitialPosition.x - 100,
                                 mBallInitialPosition.x + 250.0f);
    }
}

void GameScreen::GenerateEnemy( int enemyCount )
{
    //    auto obj = Enemy::create( );
    //    mMainLayer->addChild( obj );
    //    obj->setCameraMask( (unsigned short)CameraFlag::USER2 );
    //    obj->setPosition( 400, 130 );
    float lastHeight = 0;
    float height = cocos2d::random( mBallInitialPosition.y + 600.0f,
                                    mBallInitialPosition.y + 800.0f);
    float width = cocos2d::random( mBallInitialPosition.x - 100,
                                   mBallInitialPosition.x + 250.0f);
    for( int platformNumber = 0; platformNumber < enemyCount; platformNumber++ )
    {
        auto objplat = Enemy::create( );
        mMainLayer->addChild( objplat );
        objplat->setCameraMask( (unsigned short)CameraFlag::USER2 );
        objplat->setPosition( width, lastHeight + height );
        objplat->setLocalZOrder( 1 );
        lastHeight += height;
        height = cocos2d::random( 600.0f, 800.0f);
        width = cocos2d::random( mBallInitialPosition.x - 200,
                                 mBallInitialPosition.x + 450.0f);
        mBall->mEnemies.push_back( objplat );
    }
    
    
}

void GameScreen::Restart()
{
    log("restarting");



    mMainCamera->setPosition3D(mBallInitialPosition + Vec3(0,0,1200));
    mMainCamera->lookAt(mBallInitialPosition, Vec3(0.0,1.0,0.0));
    MyContactListener* conlis = mContactListener;
    mContactListener = new MyContactListener();
    Physics::GetInstance().GetPhysicalWorld()->SetContactListener( mContactListener );
    delete(conlis);
    Physics::GetInstance().DeleteAllBodies();
    mMainLayer->removeAllChildren();
    mMessageLayer = Layer::create();
    mMessageLayer->setLocalZOrder( 20 );
    mMainLayer->addChild( mMessageLayer );
    //    log( "children removed %i" , mBall);
    //    removeChild( mMainCamera );
    
    // create ParallaxNode
    //    mBackground = Sprite::create();
    //    mBackground->initWithFile("bg.png");
    //    mBackground->setScale(2);
    //    mBackground->setPosition( 700,0);
    //    auto middle_layer = Sprite::create();
    //    middle_layer->initWithFile("ml.png");
    
    //    auto top_layer = Sprite::create();
    //    top_layer->initWithFile("tl.png");
    
    
    mParallaxBackground = InfiniteParallaxNode::create();
    mParallaxBackground->Initialize();
    // background image is moved at a ratio of 0.4x, 0.5y
    //    mParallaxBackground->addChild(mBackground, -1, Vec2(0.4f,0.5f), Vec2::ZERO);
    mParallaxBackground->AddBackgroundLayer( "bg_blue.png", -1, Vec2(0.1f,0.99f), false,
                                             BGA_LEFT );
    mParallaxBackground->AddBackgroundLayer( "bg2.png", 1, Vec2(1.0f,-0.04f), false,
                                             BGA_RIGHT);
    //    mParallaxBackground->setPositionX( mVisibleSize.width/2  );
    
    //    // tiles are moved at a ratio of 2.2x, 1.0y
    //    mBackground->addChild(middle_layer, 1, Vec2(2.2f,1.0f), Vec2(0,0) );
    
    //    // top image is moved at a ratio of 3.0x, 2.5y
    //    mBackground->addChild(top_layer, 2, Vec2(3.0f,1.5f), Vec2(170,0) );
    
    mParallaxBackground->setCameraMask((unsigned short)CameraFlag::USER2);
    mMainLayer->addChild(mParallaxBackground);

    GameObject* ground = GameObject::create( "ground.ini" );
    ground->setPosition( 0, mBallInitialPosition.y - 100 );
    ground->setTag( OT_GROUND );
    AddToMainLayer( ground );

    MessageBalloon* msg = MessageBalloon::create();
    msg->setPosition( mBallInitialPosition.x + 200, mBallInitialPosition.y + 200 );
    mMessageLayer->addChild( msg );
    msg->setCameraMask( (unsigned short)CameraFlag::USER2 );
    msg->SetText( "Tap on little beetle!" );

    mBall = Player::create( );
    mBall->setCameraMask( (unsigned short)CameraFlag::USER2 );
    mBall->setPosition( mBallInitialPosition.x, mBallInitialPosition.y );
    mBall->setLocalZOrder( 10 );
    //    GeneratePlatforms( 20 );
    mPlatformManager->Restart();

    GenerateFuel( 30 );
    GenerateEnemy( 6 );
    mMainLayer->addChild( mBall );

    mParallaxForground = InfiniteParallaxNode::create();
    mParallaxForground->Initialize();
    // background image is moved at a ratio of 0.4x, 0.5y
    //    mParallaxBackground->addChild(mBackground, -1, Vec2(0.4f,0.5f), Vec2::ZERO);
    mParallaxForground->AddBackgroundLayer( "fg.png", 1, Vec2(0.8f,0.8f), false,
                                            BGA_RIGHT);
    mParallaxForground->setCameraMask((unsigned short)CameraFlag::USER2);
    mParallaxForground->setLocalZOrder( 1000 );
    mMainLayer->addChild(mParallaxForground);
    //    log( "ball has been created %i", mBall);
    //mBall->getPhysicsBody()->applyForce( Vect( 0, 1000) );
    // Create contact listener
    
    mAcceleration = Vec3( 0, 0, 0 );
    
    //    mMainCamera = Camera::createPerspective(60, (float)mVisibleSize.width/mVisibleSize.height,
    //                                            1.0, 1500);
    //    mMainCamera->setCameraFlag(CameraFlag::USER2);
    //    //the calling order matters, we should first call setPosition3D, then call lookAt.
    



}

void GameScreen::ProcessContacts()
{
    
    mBall->ProcessContactStartList();
    mBall->ProcessContactFinishedList();
}



void GameScreen::AddToHUDLayer( Node* node)
{
    mHUDLayer->addChild( node );
    node->setCameraMask( (unsigned short)CameraFlag::USER1 );
}

void GameScreen::RemoveFromMainLayer( Node* node)
{
    if( node != nullptr )
    {
        //        log("removing");
        //        mScheduledRemove.push_back( node );
        //        log("removed");
    }
}
