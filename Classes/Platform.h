#ifndef _PLATFORM_H
#define _PLATFORM_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class Platform: public GameObject
{
public:
    Platform();
    virtual void SetBits();
    static Platform* create( /*std::string info_path*/ );
    virtual void PlayerJumpedOn( );
    virtual bool IsStrong() { return true; }
    ~Platform(){}
};

class FragilePlatform: public Platform
{
public:
    static FragilePlatform* create( /*std::string info_path*/ );
    virtual bool IsStrong() { return false; }
    virtual void PlayerJumpedOn( );
};

#endif //_PLATFORM_H
