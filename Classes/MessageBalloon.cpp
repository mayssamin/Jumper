#include "MessageBalloon.h"

MessageBalloon::MessageBalloon()
{
    mFontFamily = "fonts/BNAZANIN.TTF";
    mFontSize = 49;
}

void MessageBalloon::SetBits()
{
    setTag(OT_MESSAGE);
}

MessageBalloon* MessageBalloon::create( )
{
    MessageBalloon* msg = new MessageBalloon( );

    if ( msg->initWithFile( "message.ini" ) )
    {
        msg->SetBits();
        std::string property = msg->GetPropertyFirstValue("special", "tail_sprite");
        if( property != "" )
        {
            trim(property);
            msg->mMessageTail = Sprite::createWithSpriteFrameName( property.c_str() );
            msg->mMessageTail->setScale( msg->mSprite->getScale() );
            msg->addChild( msg->mMessageTail );
        }
        property = msg->GetPropertyFirstValue("special", "tail_start");
        if( property != ""  &&  msg->mMessageTail != nullptr )
        {
            int index = property.find(",");
            if( index > -1 )
            {
                std::string first = property.substr( 0, index);
                trim(first);
                std::string second = property.substr( index+1 );
                trim(second);
                float offsetX = atof(first.c_str() );
                float offsetY = atof(second.c_str() );
                //log(" %s %f", first.c_str(),  atof(second.c_str() ));
                Vec2 tailOffset(msg->mSprite->getBoundingBox().size.width*offsetX,
                                msg->mSprite->getBoundingBox().size.height*offsetY);

                msg->mMessageTail->setPosition( tailOffset.x, tailOffset.y );
            }
        }
        property = msg->GetPropertyFirstValue("special", "tail_rotation");
        if( property != ""  &&  msg->mMessageTail != nullptr )
        {
            trim(property);
            msg->mMessageTail->setRotation( atof( property.c_str() ) );

        }
        property = msg->GetPropertyFirstValue("special", "tail_flipped");
        if(property !="" &&  msg->mMessageTail != nullptr)
        {
            trim(property);
            if( ( property == "yes" || property == "true" )  )
            {
                msg->mMessageTail->setScaleX( -msg->mMessageTail->getScaleX() );

            }
        }

        property = msg->GetPropertyFirstValue("special", "font_family");
        if( property != ""  )
        {
            trim(property);
            msg->mFontFamily = property;

        }
        property = msg->GetPropertyFirstValue("special", "font_size");
        if( property != ""  )
        {
            trim(property);
            msg->mFontSize = atoi( property.c_str() ) ;
        }
        msg->mText = Label::createWithSystemFont(
                    "text",
                    msg->mFontFamily, msg->mFontSize);
        msg->mText->setColor(Color3B::ORANGE);
        msg->addChild(msg->mText );


        msg->autorelease();

        return msg;
    }

    CC_SAFE_DELETE( msg );
    return NULL;
}

void MessageBalloon::SetText( string text )
{

    mText->setString( text );
}
