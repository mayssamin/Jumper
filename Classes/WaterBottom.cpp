#include "WaterBottom.h"
#include "GameLayer.h"
WaterBottom::WaterBottom()
{

}

void WaterBottom::SetBits()
{
    setTag(OT_WATERBOTTOM);
}

WaterBottom* WaterBottom::create( )
{
    WaterBottom* btm = new WaterBottom( );

    if ( btm->initWithFile( "water_bottom_main.ini" ) )
    {
        btm->SetBits();
        std::string property = btm->GetPropertyFirstValue("special", "tiles_number");
        if( property != "" )
        {
            trim(property);
            btm->mTilesTypeNumber = atoi( property.c_str() );
        }
        property = btm->GetPropertyFirstValue("special", "tiles_name_prefix");
        if( property != "" )
        {
            trim(property);
            btm->mTilesNamePrefix = property;
        }
        property = btm->GetPropertyFirstValue("special", "tiles_name_postfix");
        if( property != "" )
        {
            trim(property);
            btm->mTilesNamePostfix = property;
        }
        btm->CreateTilesAndUpdateSize();
        btm->autorelease();

        return btm;
    }

    CC_SAFE_DELETE( btm );
    return NULL;
}

void WaterBottom::CreateTilesAndUpdateSize()
{
    auto mainLayer =
            (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    ManageableCamera* mainCam = mainLayer->GetMainCamera();

    Rect screenSize = mainCam->GetVisibleRect();

    int randNum = cocos2d::random( 1, mTilesTypeNumber );

    float width = 0;
    Node* parent = Node::create();
    Sprite* tile = nullptr;
    while( width < screenSize.size.width )
    {
        tile = Sprite::create();
        string name = mTilesNamePrefix + to_string( randNum ) + mTilesNamePostfix;
        tile->initWithSpriteFrameName( name );
        tile->setPosition( width,0);
        width += tile->getBoundingBox().size.width;
        parent->addChild( tile );
        randNum = cocos2d::random( 1, mTilesTypeNumber );
    }

    mBoundingSize.x = width;
    if( tile != nullptr )
    {
        mBoundingSize.y = tile->getBoundingBox().size.height;
        parent->setPosition( -width*.5 + tile->getBoundingBox().size.width*.5, 0);

        AddPhysicalBody("auto_box","static","","","1,1,1");

    }
    addChild( parent );

}
