#ifndef __SETTING_H__
#define __SETTING_H__

class Setting
{
private:
    Setting(){}
public:
    float SCALE_RATIO = 32.0;

public:
    static Setting& GetInstance()
    {
        static Setting instance;
        return instance;
    }
};

#endif
