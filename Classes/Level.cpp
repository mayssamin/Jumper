#include "Level.h"

Level::Level()
{
    mHeight = 10000;
}

bool Level::Initialize()
{
    mWaterBottom = WaterBottom::create();
    mWaterBottom->setCameraMask( (unsigned short)CameraFlag::USER2  );
    mWaterBottom->setPosition3D( Vec3(0,0,0) );
    addChild( mWaterBottom );

    mWaterSurface = WaterSurface::create();
    mWaterSurface->setCameraMask( (unsigned short)CameraFlag::USER2  );
    mWaterSurface->setPosition3D( Vec3(0,mHeight,0) );
    addChild( mWaterSurface );

    return true;
}

Level* Level::create( )
{
    Level* lvl = new Level( );

    if ( lvl->Initialize() )
    {

        lvl->autorelease();
        return lvl;
    }

    CC_SAFE_DELETE( lvl );
    return NULL;
}

GameObject* Level::GetBottomObject()
{
    return mWaterBottom;
}
GameObject* Level::GetSurfaceObject()
{
    return mWaterSurface;
}
