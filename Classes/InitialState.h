#ifndef _INITIALSTATE_H
#define _INITIALSTATE_H

#include "cocos2d.h"
#include "StateMachine.h"

using namespace cocos2d;

class InitialState: public State
{
public:
    InitialState();
    ~InitialState(){}
    virtual void OnEnter( State* previous_state );
    virtual void OnExit( State* next_state );
};

#endif //_INITIALSTATE_H
