#ifndef _WATERBOTTOM_H
#define _WATERBOTTOM_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class WaterBottom : public GameObject
{
private:
    int mTilesTypeNumber = 1;
    string mTilesNamePrefix;
    string mTilesNamePostfix;
    void CreateTilesAndUpdateSize();
public:
    WaterBottom();
    virtual void SetBits();
    static WaterBottom* create();
    ~WaterBottom(){}
};

#endif //_WATERBOTTOM_H
