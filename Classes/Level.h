#ifndef _LEVEL_H
#define _LEVEL_H

#include "cocos2d.h"
#include "WaterBottom.h"
#include "WaterSurface.h"

using namespace cocos2d;

class Level : public Node
{
private:
    float mHeight;
    WaterBottom* mWaterBottom;
    WaterSurface* mWaterSurface;
public:
    GameObject* GetBottomObject();
    GameObject* GetSurfaceObject();
    Level();
    bool Initialize();
    static Level* create();
    ~Level(){}
};

#endif //_LEVEL_H
