#include "Fish.h"
#include "GameLayer.h"

Fish::Fish()
{

}

void Fish::SetBits()
{
    setTag(OT_FISH);
}

Fish* Fish::create()
{
    Fish* fish = new Fish( );
    if ( fish->initWithFile( "fish.ini" ) )
    {
        fish->SetBits();


        fish->autorelease();

        return fish;
    }

    CC_SAFE_DELETE( fish );
    return NULL;
}

void Fish::Update( float dt)
{
    if(mResting)
    {
//        log("$$$$ rest %f dt %f", mRestRemainTime, dt );
        mRestRemainTime -= dt;
        if( mRestRemainTime <= 0 )
        {
            mResting = false;
            mRestRemainTime = -1;
            if( mSwimSpeedX < 0 )
            {
                StopAnimation("rest_left", false);
                PlayAnimation( "swim_left", true );
            }
            else
            {
                StopAnimation("rest_right", false);
                PlayAnimation( "swim_right", true );
            }
        }
        return;
    }
    if( mIsExposed )
    {
        ///TODO: Optimization needed
        GameLayer* gscr =
                (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
        ManageableCamera* mainCam = gscr->GetMainCamera();
        Rect visibleRect = mainCam->GetVisibleRect( getPositionZ() );
        if( getPositionX() > ( visibleRect.origin.x + visibleRect.size.width * 1.5 ) ||
            getPositionX() < visibleRect.origin.x - visibleRect.size.width )
        {
            removeFromParent();
            return;
        }

        int randNum = cocos2d::random(1,100);
//        log("rand %i", randNum);
        if( !mResting && randNum < 5 && mRestRemainTime != -1 )
        {
            mResting = true;
            mRestRemainTime = 3 ;
            if( mSwimSpeedX < 0 )
            {
                StopAnimation("swim_left", false);
                PlayAnimation( "rest_left", true );
            }
            else
            {
                StopAnimation("swim_right", false);
                PlayAnimation( "rest_right", true );
            }

//            log("^^^^ %i", mRestEndTime );
        }
        else
        {
            setPositionX( getPositionX() + mSwimSpeedX );
        }

    }
    else
    {
        if(mIsStartedSwiming)
        {
            setPositionX( getPositionX() + mSwimSpeedX );
            ///TODO: Optimization needed
            GameLayer* gscr =
                    (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
            ManageableCamera* mainCam = gscr->GetMainCamera();
            Rect visibleRect = mainCam->GetVisibleRect( getPositionZ() );
            if( getPositionX() < ( visibleRect.origin.x + visibleRect.size.width ) &&
                getPositionX() >visibleRect.origin.x      )
            {
                mIsExposed = true;
            }
        }
        else
        {
            GameLayer* gscr =
                    (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
            ManageableCamera* mainCam = gscr->GetMainCamera();
            Rect visibleRect = mainCam->GetVisibleRect( getPositionZ() );
            if( getPositionX() >= ( visibleRect.origin.x + visibleRect.size.width ) )
            {
                PlayAnimation("swim_left", true);
                mSwimSpeedX = -20;
            }
            else
            {
                PlayAnimation("swim_right", true);
                mSwimSpeedX = 20;
            }
            mIsStartedSwiming = true;
        }
    }
}

void Fish::Start()
{
    this->schedule( schedule_selector( Fish::Update ) , .033 );
}
