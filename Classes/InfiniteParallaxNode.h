#ifndef _INFINITEPARALLAXNODE_H
#define _INFINITEPARALLAXNODE_H

#include "cocos2d.h"
using namespace std;
using namespace cocos2d;

enum BackgroundLayerAlignment
{
    BGA_CENTER = 0,
    BGA_LEFT = 1,
    BGA_RIGHT = 2
};

class BackgroundLayer : public Node
{
private:
    int mVerticalChildrenNumber;
public:
    CREATE_FUNC(BackgroundLayer);
    int GetVerticalChildrenNumber() { return mVerticalChildrenNumber; }
    void SetVerticalChildrenNumber( int childNum ){ mVerticalChildrenNumber = childNum; }
};

class InfiniteParallaxNode : public ParallaxNode
{
private:
    float mScreenCoef = 1;
    vector<BackgroundLayer*> mLayers;
    Vec3 mBottomLeft, mTopRight;
public:
    static InfiniteParallaxNode* create();
    void Initialize();
    virtual void visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags) override;
    void AddBackgroundLayer(string fileName, int zOrder,
                            Vec2 parallaxRatio, bool fillHorizontal,
                            BackgroundLayerAlignment bga);
};

#endif //_INFINITEPARALLAXNODE_H
