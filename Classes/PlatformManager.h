#ifndef _PLATFORMMANAGER_H
#define _PLATFORMMANAGER_H

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;
class PlatformGroup : public Node
{
protected:
    float mHeight;
    Vec2 mOffset;    //I used offset because node's setposition did not update box2d info
    Vec2 mStemEnd;
public:
    float GetHeight(){ return mHeight;}
    virtual void Generate( float hardness, float min_height, Vec2 offset , Vec2 prev_stem_end) = 0;
    Vec2 GetOffset(){ return mOffset;}
    void RemovePlatforms();
    Vec2 GetStemEnd() { return mStemEnd; }
};

class SimplePlatforms : public PlatformGroup
{
public:
    CREATE_FUNC(SimplePlatforms);
    void Generate(float hardness, float min_height , Vec2 offset, Vec2 prev_stem_end);
};

class SimpleWithFragilePlatforms : public PlatformGroup
{
public:
    CREATE_FUNC(SimpleWithFragilePlatforms);
    void Generate(float hardness, float min_height , Vec2 offset, Vec2 prev_stem_end);
};

class PlatformManager
{
private:
    float mLastHeight = 0;
    Vec2 mLastStemEnd;
    PlatformGroup* mCurrentGroup = nullptr;
    vector<PlatformGroup*> mPreviousGroups;
public:
    PlatformManager();
    ~PlatformManager(){}
    void Restart();
    void Update();
};

#endif //_PLATFORMMANAGER_H
