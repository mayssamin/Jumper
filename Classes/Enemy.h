#ifndef _ENEMY_H
#define _ENEMY_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class Enemy: public GameObject
{
private:
    bool mIsDangerous = true;
public:
    Enemy();
    virtual void SetBits();
    static Enemy* create( );
    ~Enemy(){}
    void SetIsDangerous( bool is_dangerous);
    bool GetIsDangerous();

};

#endif //_ENEMY_H
