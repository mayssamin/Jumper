#include "WaterSurface.h"
#include "GameLayer.h"

WaterSurface::WaterSurface()
{

}

void WaterSurface::SetBits()
{
    setTag(OT_WATERSURFACE);
}

WaterSurface* WaterSurface::create( )
{
    WaterSurface* surf = new WaterSurface( );

    if ( surf->initWithFile( "water_surface_main.ini" ) )
    {
        surf->SetBits();
        std::string property = surf->GetPropertyFirstValue("special", "tiles_number");
        if( property != "" )
        {
            trim(property);
            surf->mTilesTypeNumber = atoi( property.c_str() );
        }
        property = surf->GetPropertyFirstValue("special", "tiles_name_prefix");
        if( property != "" )
        {
            trim(property);
            surf->mTilesNamePrefix = property;
        }
        property = surf->GetPropertyFirstValue("special", "tiles_name_postfix");
        if( property != "" )
        {
            trim(property);
            surf->mTilesNamePostfix = property;
        }
        surf->CreateTilesAndUpdateSize();
        surf->autorelease();

        return surf;
    }

    CC_SAFE_DELETE( surf );
    return NULL;
}

void WaterSurface::CreateTilesAndUpdateSize()
{
    auto mainLayer =
            (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    ManageableCamera* mainCam = mainLayer->GetMainCamera();

    Rect screenSize = mainCam->GetVisibleRect();

    int randNum = cocos2d::random( 1, mTilesTypeNumber );

    float width = 0;
    Node* parent = Node::create();
    Sprite* tile = nullptr;
    while( width < screenSize.size.width )
    {
        tile = Sprite::create();
        string name = mTilesNamePrefix + to_string( randNum ) + mTilesNamePostfix;
        tile->initWithSpriteFrameName( name );
        tile->setPosition( width,0);
        width += tile->getBoundingBox().size.width;
        parent->addChild( tile );
        randNum = cocos2d::random( 1, mTilesTypeNumber );
    }

    mBoundingSize.x = width;
    if( tile != nullptr )
    {
        mBoundingSize.y = tile->getBoundingBox().size.height;
        parent->setPosition( -width*.5 + tile->getBoundingBox().size.width*.5, 0);

        AddPhysicalBody("auto_box","static","yes","","1,1,1");

    }
    addChild( parent );

}

