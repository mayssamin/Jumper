#include "utils.h"
#include <iostream>
#include <iomanip>

string to_string_with_precision(const float a_value, const int percision )
{
    stringstream stream;
    stream << fixed << setprecision(percision) << a_value;
    return stream.str();
}

void trim(string& str)
{
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    str = str.substr(first, (last-first+1));
}

const vector<string> explode(const string& s, const char& c)
{
    string buff{""};
    vector<string> v;

    for(auto n:s)
    {
        if(n != c) buff+=n; else
            if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);

    return v;
}

Vec2 WorldToScreen( Mat4 projectionMatrix, Mat4 viewMatrix, Vec3 worldPosition,
                    Vec2 viewOffset, Vec2 viewSize)
{
    Vec4 clipSpacePos = projectionMatrix *
            ( viewMatrix * Vec4(worldPosition.x, worldPosition.y,
                                worldPosition.z,1.0) );

    Vec3 ndcSpacePos = Vec3( clipSpacePos.x / clipSpacePos.w, clipSpacePos.y / clipSpacePos.w
                             , clipSpacePos.z / clipSpacePos.w);

    //log("W1 %f", clipSpacePos.w);
    return Vec2 ( ((ndcSpacePos.x + 1.0)/ 2.0) * viewSize.x + viewOffset.x,
                  ((ndcSpacePos.y + 1.0)/ 2.0) * viewSize.y + viewOffset.y ) ;
}

Vec3 ScreenToWorld( Mat4 projectionMatrix, Mat4 viewMatrix, Vec2 screenPosition,
                    Vec2 viewOffset, Vec2 viewSize )
{

    Vec3 ndcSpacePos( (screenPosition.x - viewOffset.x)*2.0f/(viewSize.x) -1.0f,
                      (screenPosition.y - viewOffset.y)*2.0f/(viewSize.y) -1.0f,0);
    Mat4 projView = viewMatrix* projectionMatrix ;
    projView.inverse();
    //    log("talaf %f %f %f", ndcSpacePos.x,ndcSpacePos.y, ndcSpacePos.z);
    Vec4 clipSpacePos =  projView * Vec4(ndcSpacePos.x, ndcSpacePos.y,
                                         ndcSpacePos.z,-1.0) ;

    log("W2 %f", clipSpacePos.w);
    return Vec3( ndcSpacePos.x * clipSpacePos.w, ndcSpacePos.y * clipSpacePos.w
                 , ndcSpacePos.z * clipSpacePos.w) ;



    //sprintf(strTitle,"%f %f %f / %f,%f,%f ",m_pCamera->m_vPosition.x,m_pCamera->m_vPosition.y,m_pCamera->m_vPosition.z,pos.x,pos.y,pos.z);

    //SetWindowText(this->GetWindowHWND(),strTitle);

}
