#ifndef _GAMEOBJECT_H
#define _GAMEOBJECT_H

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "setting.hpp"
#include "utils.h"
#include "simpleini/SimpleIni.h"
using namespace cocos2d;

enum ObjectType
{
    OT_NONE = 0x0000,
    OT_PLAYER = 0x0001,
    OT_PLATFORM = 0x0002,
    OT_FUEL = 0x0004,
    OT_ENEMY = 0x0008,
    OT_GROUND = 0x0010,
    OT_MESSAGE = 0x0020,
    OT_WATERBOTTOM = 0x0040,
    OT_WATERSURFACE = 0x0080,
    OT_FISH = 0x00F0,
    OT_ALL = 0x00FF

};

class GameObject : public Node
{
private:
    CSimpleIniA mProperties;
    void CreateAnimations();
    void CreateAnimation(string anim);
protected:
    Vec2 mBoundingSize;
    string mDefaultFrameName ;
    map<string, Animate*> mAnimations;
    Sprite* mSprite = nullptr;
    b2Body *mPhysicalBody = nullptr;
    b2Shape *mShape;
    b2BodyDef *mBodyDef;
    float mMaxHeightTraveled = 0;
    bool InitializeStaticSprite();
    bool InitializeAnimatedSprite();
    bool InitializePhysics();
    bool InitSimple();
    void InitComplicated();
    void PlayAnimation(string animation_name, bool repeat = false);
    void StopAnimation(string animation_name, bool set_to_default = true);
    bool mIsSimple;
    vector<GameObject*> mChildrenObjects;
public:
    string GetPropertyFirstValue( string section, string key );
    vector<string> GetPropertyValues( string section, string key );
    Vec2 GetBoundingSize();
    GameObject();
    virtual void SetBits() {}
//    ~GameObject();
    static GameObject* create( string info_path );
    bool initWithFile( std::string info_path );
    virtual void setPosition(float x, float y);
    virtual void setRotationWithPhysics(float rotation);
    b2Body* GetPhysicalBody();
    void ComputeAABB( b2AABB& aabb);
    void AddCollidable(ObjectType objectType );
    void RemoveCollidable( ObjectType objectType );
    Sprite *GetSprite() const;
    void SetSprite(Sprite *sprite);
    void AddPhysicalBody(string collider_type, string body_type, string is_sensor, string initial_velocity, string material);
};

#endif //_GAMEOBJECT_H
