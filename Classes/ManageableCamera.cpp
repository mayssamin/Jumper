#include "ManageableCamera.h"
#include "utils.h"

ManageableCamera::ManageableCamera( ) : Camera()
{

}

void ManageableCamera::ZoomAround( GameObject* subject, float margin_radius)
{
    float screenWidth = Director::getInstance()->getVisibleSize().width;
    float screenHeight= Director::getInstance()->getVisibleSize().height;

    float boundingCircle = Vec2( subject->GetBoundingSize().x,
                                 subject->GetBoundingSize().y ).length();

    boundingCircle += margin_radius;

    float littleSide = screenWidth > screenHeight ? screenHeight : screenWidth;
    float zPrime = getPositionZ()* boundingCircle / littleSide;
    log("zprime %f ", zPrime);
    setPosition3D( Vec3( subject->getPositionX(), subject->getPositionY(), zPrime ) );
    lookAt( subject->getPosition3D() );
}

//float ManageableCamera::ZoomToObjectsBounds( vector<GameObject*> objects)
//{

//}

Vec3 ManageableCamera::GetScreenToWorld( Vec2 location, float z )
{
    Size viewSize = Director::getInstance()->getVisibleSize();
    //    location.y -= viewSize.height;
    Vec3 nearP(location.x, viewSize.height - location.y , 0),
            farP(location.x, viewSize.height - location.y, 1);

    nearP = unproject(nearP);
    farP = unproject(farP);
    Vec3 dir(farP - nearP);

    Plane zPlane;
    zPlane.initPlane(Vec3(0,0,1),Vec3(0,0,z));
    Ray ray;
    ray.set( nearP, dir);
    Vec3 rayint = ray.intersects(zPlane);
    return rayint;
}

Vec2 ManageableCamera::GetWorldToScreen( Vec3 worldPosition )
{
    Size viewSize = Director::getInstance()->getVisibleSize();
    Vec2 proj = project( worldPosition );
    return Vec2( proj.x, viewSize.height - proj.y );
}


ManageableCamera* ManageableCamera::create(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
{
    ManageableCamera* cam = new ManageableCamera();
    bool res = cam->initPerspective( fieldOfView, aspectRatio,nearPlane, farPlane);

    if ( res )
    {
        cam->autorelease();
        return cam;
    }

    CC_SAFE_DELETE( cam );
    return NULL;

}

Rect ManageableCamera::GetVisibleRect( float z )
{
    Rect result;
    float screenWidth = Director::getInstance()->getVisibleSize().width;
    float screenHeight= Director::getInstance()->getVisibleSize().height;

    Vec3 zeroPos = GetScreenToWorld( Vec2( 0, 0 ), z);

    Vec3 endPos = GetScreenToWorld( Vec2( screenWidth, screenHeight ), z);

    result.setRect( zeroPos.x, zeroPos.y,
                    fabs(endPos.x - zeroPos.x), fabs(endPos.y - zeroPos.y) );

    return result;
}

void ManageableCamera::AlignWithObject( GameObject* subject,
                                        VerticalAlignment vert_align,
                                        HorizontalAlignment hor_align )
{
    Rect cameraRect = GetVisibleRect( subject->getPositionZ() );
    log(" rect %f %f %f %f ", cameraRect.origin.x, cameraRect.origin.y,
        cameraRect.size.width, cameraRect.size.height );
    float subj_w = subject->GetBoundingSize().x;
    float subj_h= subject->GetBoundingSize().y;
    log("subj size %f %f", subj_w, subj_h );
    float dh = 0, dw = 0;

    if( vert_align == VA_INNER_BOTTOM )
    {
        dh = (subject->getPositionY() - subj_h/2 ) - cameraRect.origin.y;
        log( " dh u %f", dh );
    }
    else if( vert_align == VA_INNER_UP )
    {
        dh = (subject->getPositionY() + subj_h/2 ) -
                ( cameraRect.origin.y + cameraRect.size.height );
        log( " dh b %f", dh );
    }
    else if( vert_align == VA_MIDDLE )
    {
        dh = subject->getPositionY() - getPositionY();
        log( " dh m %f", dh );
    }

    if( hor_align == HA_INNER_LEFT )
    {
        dw = (subject->getPositionX() - subj_w/2 ) - cameraRect.origin.x;
        log( " dw l %f", dw );
    }
    else if( hor_align == HA_INNER_RIGHT )
    {
        dw = (subject->getPositionX() + subj_w/2 ) -
                ( cameraRect.origin.x + cameraRect.size.width );
        log( " dw r %f", dw );

    }
    else if( hor_align == HA_MIDDLE )
    {
        dw = subject->getPositionX() - getPositionX();
        log( " dw m %f", dw );
    }

    setPosition3D( getPosition3D() + Vec3(dw, dh, 0 ) );
    log( "pos %f, %f, %f", getPositionX(), getPositionY(), getPositionZ() );
    lookAt( mTargetPosition + Vec3(dw, dh, 0 ) );
}

void ManageableCamera::lookAt(const Vec3& lookAtPos, const Vec3& up)
{
    Camera::lookAt(lookAtPos, up);
    mTargetPosition = lookAtPos;
}

