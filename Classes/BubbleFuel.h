#ifndef _BUBBLEFUEL_H
#define _BUBBLEFUEL_H

#include "cocos2d.h"
#include "GameObject.h"

using namespace cocos2d;

class BubbleFuel: public GameObject
{
public:
    BubbleFuel();
    virtual void SetBits();
    static BubbleFuel* create( /*std::string info_path*/ );
    ~BubbleFuel(){}
};

#endif //_BUBBLEFUEL_H
