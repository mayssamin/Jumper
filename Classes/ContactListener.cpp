#include "ContactListener.h"
#include "Player.h"
MyContactListener::MyContactListener() {
}

MyContactListener::~MyContactListener()
{
}

void MyContactListener::BeginContact(b2Contact* contact)
{
        b2Body *bodyA = contact->GetFixtureA()->GetBody();
        b2Body *bodyB = contact->GetFixtureB()->GetBody();
        if (bodyA->GetUserData() != nullptr && bodyB->GetUserData() != nullptr)
        {
    //        log("tags %i %i", ((GameObject*)bodyA->GetUserData())->getTag(),
    //            ((GameObject*)bodyB->GetUserData())->getTag());
            Player* ball = nullptr;
            GameObject* object = nullptr;

            if( ((GameObject*)bodyA->GetUserData())->getTag() == OT_PLAYER)
            {
                ball = (Player*) bodyA->GetUserData();
                object = (GameObject*) bodyB->GetUserData();

            }
            else if( ((GameObject*)bodyB->GetUserData())->getTag() == OT_PLAYER)
            {
                ball = (Player*) bodyB->GetUserData();
                object = (GameObject*) bodyA->GetUserData();
            }
            if( ball != nullptr )
            {
    //            log("Collision ");
                ball->CollisionWith( object );
            }
        }
}

void MyContactListener::EndContact(b2Contact* contact)
{
    b2Body *bodyA = contact->GetFixtureA()->GetBody();
    b2Body *bodyB = contact->GetFixtureB()->GetBody();
    if (bodyA->GetUserData() != nullptr && bodyB->GetUserData() != nullptr)
    {
        Player* ball = nullptr;
        GameObject* object = nullptr;
        if( ((GameObject*)bodyA->GetUserData())->getTag() == OT_PLAYER)
        {
            ball = (Player*) bodyA->GetUserData();
            object = (GameObject*) bodyB->GetUserData();
        }
        else if( ((GameObject*)bodyB->GetUserData())->getTag() == OT_PLAYER)
        {
            ball = (Player*) bodyB->GetUserData();
            object = (GameObject*) bodyA->GetUserData();
        }
        if( ball != nullptr )
        {
            ball->CollisionFinishedWith( object );
        }
    }
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
}

//void MyContactListener::ProcessBeginContactList()
//{
//    for( vector<b2Contact*>::iterator cit = mContacts.begin();
//         cit != mContacts.end(); cit++)
//    {
//        b2Contact* contact = *cit;
//        b2Body *bodyA = contact->GetFixtureA()->GetBody();
//        b2Body *bodyB = contact->GetFixtureB()->GetBody();
//        if (bodyA->GetUserData() != nullptr && bodyB->GetUserData() != nullptr)
//        {
//            //        log("tags %i %i", ((GameObject*)bodyA->GetUserData())->getTag(),
//            //            ((GameObject*)bodyB->GetUserData())->getTag());
//            Ball* ball = nullptr;
//            GameObject* object = nullptr;
//            if( ((GameObject*)bodyA->GetUserData())->getTag() == OT_PLAYER)
//            {
//                ball = (Ball*) bodyA->GetUserData();
//                object = (GameObject*) bodyB->GetUserData();

//            }
//            else if( ((GameObject*)bodyB->GetUserData())->getTag() == OT_PLAYER)
//            {
//                ball = (Ball*) bodyB->GetUserData();
//                object = (GameObject*) bodyA->GetUserData();
//            }
//            if( ball != nullptr )
//            {
//                //            log("Collision ");
//                ball->CollisionWith( object );
//            }
//        }
//    }
//    mContacts.clear();
//}

//void MyContactListener::ProcessEndContactList()
//{
//    for( vector<b2Contact*>::iterator cit = mFinishedContacts.begin();
//         cit != mFinishedContacts.end(); cit++)
//    {
//        b2Contact* contact = *cit;
//        b2Body *bodyA = contact->GetFixtureA()->GetBody();
//        b2Body *bodyB = contact->GetFixtureB()->GetBody();
//        if (bodyA->GetUserData() != nullptr && bodyB->GetUserData() != nullptr)
//        {
//            log("then here");
//            Ball* ball = nullptr;
//            GameObject* object = nullptr;
//            log("here");
//            if( ((GameObject*)bodyA->GetUserData())->getTag() == OT_PLAYER)
//            {
//                ball = (Ball*) bodyA->GetUserData();
//                object = (GameObject*) bodyB->GetUserData();
//            }
//            else if( ((GameObject*)bodyB->GetUserData())->getTag() == OT_PLAYER)
//            {
//                ball = (Ball*) bodyB->GetUserData();
//                object = (GameObject*) bodyA->GetUserData();
//            }
//            if( ball != nullptr )
//            {
//                ball->CollisionFinishedWith( object );
//            }
//        }
//    }
//    mFinishedContacts.clear();
//}
