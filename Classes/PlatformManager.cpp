#include "PlatformManager.h"
#include "Platform.h"
#include "GameLayer.h"


void PlatformGroup::RemovePlatforms()
{
    for( Vector<Node*>::iterator childit = getChildren().begin();
         childit != getChildren().end(); childit++ )
    {
        GameLayer* gscr =
                (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
        gscr->mScheduledRemove.push_back( /*(GameObject*)*/*childit );

    }
}

void SimplePlatforms::Generate(float hardness, float min_height, Vec2 offset, Vec2 prev_stem_end)
{
    mOffset = offset;
    float lastHeight = 0;
    float height = cocos2d::random( /*mBallInitialPosition.y +*/ 150.0f,
                                    /*mBallInitialPosition.y +*/ 250.0f );
    float width = cocos2d::random( /*mBallInitialPosition.x */-100.0f,
                                   /*mBallInitialPosition.x +*/ 250.0f);
    mStemEnd = prev_stem_end;
    //    int counter = 0;
    while( lastHeight < min_height )
    {
        GameObject* stem = GameObject::create( "stem.ini" );
        this->addChild( stem );
        stem->setLocalZOrder( -10 );
        auto objplat = Platform::create( );
        float rotation = cocos2d::random( -20.0f, 20.0f);
        objplat->setRotationWithPhysics( rotation );
        this->addChild( objplat );
        objplat->setCameraMask( (unsigned short)CameraFlag::USER2 );
        objplat->setPosition( width + offset.x, lastHeight + height + offset.y );
        int isFlipedCoef = 1;
        if( objplat->getPositionX() < 0 )
        {
            isFlipedCoef = -1;
            objplat->setScaleX( -objplat->getScaleX() );
        }

        Vec2 newEnd = objplat->getPosition();
        newEnd.x -= isFlipedCoef * objplat->GetBoundingSize().x/2;
        Vec2 center(objplat->getPositionX(), objplat->getPositionY());
        newEnd.rotate( center, CC_DEGREES_TO_RADIANS( -rotation ));

        //        newEnd.y -= cosf(CC_DEGREES_TO_RADIANS(rotation) ) *
        //                ( newEnd.x - objplat->getPositionX() );

        Vec2 stemPos = ( newEnd + mStemEnd )/2;
        float stemLen = ( newEnd - mStemEnd ).length();
        Vec2 horiline = Vec2( 512, mStemEnd.y ) - mStemEnd;

        float deg = Vec2::angle( (newEnd - mStemEnd), horiline );
        stem->setScaleY( stemLen / stem->GetBoundingSize().y );
        stem->setPosition( stemPos.x, stemPos.y );
        stem->setRotation(  90 - CC_RADIANS_TO_DEGREES(deg) );
        mStemEnd = newEnd;


        lastHeight += height;
        height = cocos2d::random( 250.0f, 450.0f);
        int leftOrRight = cocos2d::random( 0, 1);
        if( leftOrRight == 1 )
        {
            width = cocos2d::random( /*mBallInitialPosition.x */-300.0f,
                                     /*mBallInitialPosition.x +*/ -100.0f);

        }
        else
        {
            width = cocos2d::random( /*mBallInitialPosition.x */100.0f,
                                     /*mBallInitialPosition.x +*/ 300.0f);
        }
        //        counter++;
    }
    //    log("counter %i", counter);
    mHeight = lastHeight;
}

void SimpleWithFragilePlatforms::Generate(
        float hardness, float min_height, Vec2 offset, Vec2 prev_stem_end)
{
    mOffset = offset;
    float lastHeight = 0;
    float height = cocos2d::random( /*mBallInitialPosition.y +*/ 150.0f,
                                    /*mBallInitialPosition.y +*/ 250.0f );
    float width = cocos2d::random( /*mBallInitialPosition.x */-100.0f,
                                   /*mBallInitialPosition.x +*/ 250.0f);
    mStemEnd = prev_stem_end;
    int fragileCounter = 0;
    while( lastHeight < min_height )
    {
        GameObject* stem = GameObject::create( "stem.ini" );
        this->addChild( stem );
        stem->setLocalZOrder( -10 );
        auto objplat = Platform::create( );
        float rotation = cocos2d::random( -20.0f, 20.0f);
        objplat->setRotationWithPhysics( rotation );
        this->addChild( objplat );
        objplat->setCameraMask( (unsigned short)CameraFlag::USER2 );
        objplat->setPosition( width + offset.x, lastHeight + height + offset.y );
        int isFlipedCoef = 1;
        if( objplat->getPositionX() < 0 )
        {
            isFlipedCoef = -1;
            objplat->setScaleX( -objplat->getScaleX() );
        }

        Vec2 newEnd = objplat->getPosition();
        newEnd.x -= isFlipedCoef * objplat->GetBoundingSize().x/2;
        Vec2 center(objplat->getPositionX(), objplat->getPositionY());
        newEnd.rotate( center, CC_DEGREES_TO_RADIANS( -rotation ));

        //        newEnd.y -= cosf(CC_DEGREES_TO_RADIANS(rotation) ) *
        //                ( newEnd.x - objplat->getPositionX() );

        Vec2 stemPos = ( newEnd + mStemEnd )/2;
        float stemLen = ( newEnd - mStemEnd ).length();
        Vec2 horiline = Vec2( 512, mStemEnd.y ) - mStemEnd;

        float deg = Vec2::angle( (newEnd - mStemEnd), horiline );
        stem->setScaleY( stemLen / stem->GetBoundingSize().y );
        stem->setPosition( stemPos.x, stemPos.y );
        stem->setRotation(  90 - CC_RADIANS_TO_DEGREES(deg) );

        if( fragileCounter < 2 )
        {
            int randNum = cocos2d::random( 1, 100 );
            if( randNum < 30 )
            {
                auto objplat_frag = FragilePlatform::create( );
                float fragRotation = cocos2d::random( -20.0f, 20.0f);
                objplat_frag->setRotationWithPhysics( fragRotation );
                this->addChild( objplat_frag );
                objplat_frag->setCameraMask( (unsigned short)CameraFlag::USER2 );
                objplat_frag->setPosition( stemPos.x, stemPos.y );
                int isFlipedCoef = 1;

                if( objplat_frag->getPositionX() < 0 )
                {
                    isFlipedCoef = -1;
                    objplat_frag->setScaleX( -objplat_frag->getScaleX() );
                }
                Vec2 fragEnd = objplat_frag->getPosition();
                fragEnd.x -= isFlipedCoef * objplat_frag->GetBoundingSize().x/2;
                Vec2 center(objplat_frag->getPositionX(), objplat_frag->getPositionY());
                fragEnd.rotate( center, CC_DEGREES_TO_RADIANS( -fragRotation ));

                Vec2 diff = objplat_frag->getPosition() - fragEnd;
                fragEnd += diff;
                Vec2 fragNewPos = (objplat_frag->getPosition() + diff);
                objplat_frag->setPosition( fragNewPos.x, fragNewPos.y );

            }
        }

        mStemEnd = newEnd;


        lastHeight += height;
        height = cocos2d::random( 250.0f, 450.0f);
        int leftOrRight = cocos2d::random( 0, 1);
        if( leftOrRight == 1 )
        {
            width = cocos2d::random( /*mBallInitialPosition.x */-300.0f,
                                     /*mBallInitialPosition.x +*/ -100.0f);

        }
        else
        {
            width = cocos2d::random( /*mBallInitialPosition.x */100.0f,
                                     /*mBallInitialPosition.x +*/ 300.0f);
        }
        //        counter++;
    }
    //    log("counter %i", counter);
    mHeight = lastHeight;
}

PlatformManager::PlatformManager()
{

}


void PlatformManager::Restart()
{

    GameLayer* gscr =
            (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );

    GameObject* WaterBottom = gscr->GetCurrentLevel()->GetBottomObject();
    GameObject* waterSurface = gscr->GetCurrentLevel()->GetSurfaceObject();
    Rect scrRect = gscr->GetMainCamera()->GetVisibleRect();

    mLastHeight = gscr->GetCurrentLevel()->GetBottomObject()->getPositionY();
    mPreviousGroups.clear();
    float dist2Surface = waterSurface->getPositionY() - mLastHeight;
    mLastStemEnd = Vec2( 0.0f, 0);
    while ( dist2Surface > scrRect.size.height/5 )
    {
        float groupHeight = dist2Surface > scrRect.size.height ? scrRect.size.height : dist2Surface;
        mCurrentGroup = SimplePlatforms::create();
        mCurrentGroup->Generate( 0, groupHeight, Vec2(0, 0 ), mLastStemEnd );
        mCurrentGroup->setCameraMask( (unsigned short)CameraFlag::USER2 );
        mCurrentGroup->setPosition( 0, mLastHeight );
        mLastHeight += mCurrentGroup->GetHeight();
        gscr->AddToMainLayer( mCurrentGroup );
        mPreviousGroups.push_back( mCurrentGroup );
        dist2Surface = waterSurface->getPositionY() - mLastHeight;
        mLastStemEnd = mCurrentGroup->GetStemEnd();
        mLastStemEnd.y = 0;
    }
}

void PlatformManager::Update()
{
    GameLayer* gscr =
            (GameLayer*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    ManageableCamera* mainCam = gscr->GetMainCamera();
    Rect visibleRect = mainCam->GetVisibleRect();

    for( vector<PlatformGroup*>::iterator platit = mPreviousGroups.begin();
         platit != mPreviousGroups.end(); platit++ )
    {
        PlatformGroup* group = *platit;
        Rect groupRect( visibleRect.origin.x, group->getPositionY(),
                        visibleRect.size.width, group->GetHeight() );
        if(  visibleRect.intersectsRect( groupRect ) )
        {
            group->setVisible( true );
        }
        else
        {
            group->setVisible( false );
        }
    }
    //    if( mainCam->getPositionY() > mLastHeight - 800 )
    //    {
    //        if( mCurrentGroup != nullptr )
    //        {
    //            mPreviousGroups.push_back( mCurrentGroup );
    //            mLastStemEnd = mCurrentGroup->GetStemEnd();
    //            //            log( "###### %f %f",mLastStemEnd.x, mLastStemEnd.y);
    //        }
    //        int randNum = cocos2d::random( 0, 100);
    //        if( randNum > 400 )
    //        {
    //            mCurrentGroup = SimplePlatforms::create();
    //            mCurrentGroup->Generate( 0, 1500, Vec2(0, mLastHeight), mLastStemEnd );
    //            mCurrentGroup->setCameraMask( (unsigned short)CameraFlag::USER2 );
    //            mLastHeight += mCurrentGroup->GetHeight();
    //            gscr->AddToMainLayer( mCurrentGroup );
    //        }
    //        else
    //        {
    //            mCurrentGroup = SimpleWithFragilePlatforms::create();
    //            mCurrentGroup->Generate( 0, 1500, Vec2(0, mLastHeight), mLastStemEnd );
    //            mCurrentGroup->setCameraMask( (unsigned short)CameraFlag::USER2 );
    //            mLastHeight += mCurrentGroup->GetHeight();
    //            gscr->AddToMainLayer( mCurrentGroup );
    //        }

    //    }
    //    for( vector<PlatformGroup*>::iterator platit = mPreviousGroups.begin();
    //         platit != mPreviousGroups.end(); )
    //    {
    //        PlatformGroup* group = *platit;
    //        if( group->GetOffset().y < mainCam->getPositionY() - 3000 )
    //        {
    //            if( group != nullptr )
    //            {
    //                group->RemovePlatforms();
    //                //                group->setTag(1000);
    //                gscr->mScheduledRemove.push_back( /*(GameObject*)*/group );
    //            }
    //            //            log("group count %i", mPreviousGroups.size());
    //            platit = mPreviousGroups.erase( platit );
    //            //            log("group count after erase %i", mPreviousGroups.size());
    //        }
    //        else
    //        {
    //            platit++;
    //        }
    //    }
}
