#ifndef __UTILS_H__
#define __UTILS_H__


#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include "cocos2d.h"
USING_NS_CC;

using namespace std;

string to_string_with_precision(const float a_value, const int percision = 2);

void trim(string& str);

const vector<string> explode(const string& s, const char& c);

Vec2 WorldToScreen( Mat4 projectionMatrix, Mat4 viewMatrix, Vec3 worldPosition,
                    Vec2 viewOffset, Vec2 viewSize );
Vec3 ScreenToWorld(Mat4 projectionMatrix, Mat4 viewMatrix, Vec2 screenPosition,
                    Vec2 viewOffset, Vec2 viewSize );

#endif
