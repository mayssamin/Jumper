#include "InfiniteParallaxNode.h"
#include "GameScene.h"
#include "utils.h"
#include "ManageableCamera.h"

InfiniteParallaxNode* InfiniteParallaxNode::create()
{
    // Create an instance of InfiniteParallaxNode
    InfiniteParallaxNode* node = new InfiniteParallaxNode();
    if(node)
    {
        //        node->Initialize();
        // Add it to autorelease pool
        node->autorelease();
        return node;
    }
    else
    {
        CC_SAFE_DELETE( node );
    }
    return NULL;
}

void InfiniteParallaxNode::Initialize()
{
    GameScreen* mainLayer =
            (GameScreen*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    ManageableCamera* mainCam = mainLayer->GetMainCamera();
    Vec2 zeroPos = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                  Vec3::ZERO, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                mainLayer->GetVisibleSize().height ) );
    Vec2 hundredPos = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                     Vec3(100,0, 0), Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                       mainLayer->GetVisibleSize().height ) );
//    log("^^^^^^^^^^^ zero %f %f", zeroPos.x, zeroPos.y);
    mScreenCoef = fabs(hundredPos.x - zeroPos.x)/100;
//    log("************* ratio %f", mScreenCoef );
    float screenWidth = Director::getInstance()->getVisibleSize().width;
    float screenHeight= Director::getInstance()->getVisibleSize().height;


    float left=-4000, right=4000, up=4000, down=-4000;
    Vec3 bottomLeft((left+right)/2, (up+down)/2, 0);
    Vec2 ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                               bottomLeft, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                             mainLayer->GetVisibleSize().height ) );
    while( fabs(ress.x ) >0.0001 )
    {
//        log("#### x %f", ress.x );
        if (ress.x < 0 )
        {
            left =  (left+right)/2;
            bottomLeft.x = (left+right)/2;
        }
        else
        {
            right =  (left+right)/2;
            bottomLeft.x = (left+right)/2;
        }
        ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                          bottomLeft, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                        mainLayer->GetVisibleSize().height ) );
    }
    while( fabs(ress.y ) > 0.0001 )
    {
//        log("#### y %f", ress.y );
        if (ress.y < 0 )
        {
            down =  (up+down)/2;
            bottomLeft.y = (down+up)/2;
        }
        else
        {
            up =  (up+down)/2;
            bottomLeft.y = (down+up)/2;
        }
        ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                          bottomLeft, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                        mainLayer->GetVisibleSize().height ) );
    }
//    log("real zero pos %f %f", bottomLeft.x, bottomLeft.y);
    mBottomLeft = bottomLeft;


    left=-4000, right=4000, up=4000, down=-4000;
    Vec3 topRight((left+right)/2, (up+down)/2, 0);
    ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                               topRight, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                             mainLayer->GetVisibleSize().height ) );
    while( fabs(ress.x - screenWidth ) > 0.0001 )
    {
//        log("#### x %f", ress.x );
        if (ress.x < screenWidth )
        {
            left =  (left+right)/2;
            topRight.x = (left+right)/2;
        }
        else
        {
            right =  (left+right)/2;
            topRight.x = (left+right)/2;
        }
        ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                          topRight, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                        mainLayer->GetVisibleSize().height ) );
    }
    while( fabs(ress.y - screenHeight) > 0.0001 )
    {
//        log("#### y %f", ress.y );
        if (ress.y < screenHeight )
        {
            down =  (up+down)/2;
            topRight.y = (down+up)/2;
        }
        else
        {
            up =  (up+down)/2;
            topRight.y = (down+up)/2;
        }
        ress = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                          topRight, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                        mainLayer->GetVisibleSize().height ) );
    }
    log("real topright pos %f %f", topRight.x, topRight.y);
    mTopRight = topRight;
    Vec3 alaki = mainCam->unproject( Vec3(0, 0, 0) );
log("-------------- %f %f", alaki.x*getPositionZ(), alaki.y*getPositionZ());

//    log("############### %f, %f", pos.x, pos.y);
    log("^^^^^^^^^^^ bl %f %f", mBottomLeft.x, mBottomLeft.y);
}



void InfiniteParallaxNode::visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    //    Vec2 pos = position_;
    //    Vec2    pos = [self convertToWorldSpace:Vec2::ZERO];
    //    Vec2 pos = this->absolutePosition();
    //    if( ! pos.equals(_lastPosition) )
    //    {
    //        for( int i=0; i < _parallaxArray->num; i++ )
    //        {
    //            PointObject *point = (PointObject*)_parallaxArray->arr[i];
    //            float x = -pos.x + pos.x * point->getRatio().x + point->getOffset().x;
    //            float y = -pos.y + pos.y * point->getRatio().y + point->getOffset().y;
    //            point->getChild()->setPosition(x,y);
    //        }
    //        _lastPosition = pos;
    //    }


    ParallaxNode::visit(renderer, parentTransform, parentFlags);
    GameScreen* mainLayer =
            (GameScreen*) Director::getInstance()->getRunningScene()->getChildByTag( 100 );
    Camera* mainCam = mainLayer->GetMainCamera();
    for( vector<BackgroundLayer*>::iterator layit = mLayers.begin(); layit != mLayers.end();
         layit++ )
    {
        BackgroundLayer* layer = *layit;
        int verticalChildrenNumber = layer->GetVerticalChildrenNumber();
        Vector<Node*> children = layer->getChildren();
        int co = 0;
        for( Vector<Node*>::iterator childit = children.begin(); childit != children.end();
             childit++ )
        {
//            log("========== >>>> child num %i  <<<< ==========", co++ );
            Node* child = *childit;
//            log("############# child pos %f %f", child->getPositionX() ,
//                child->getPositionY());
            Vec2 absPos = layer->convertToWorldSpace( child->getPosition() );
//            log("############# child abs pos %f %f", absPos.x , absPos.y );

            Vec3 top( absPos.x,
                      child->getBoundingBox().size.height/2 + absPos.y,0);

            Vec2 topPos = WorldToScreen( mainCam->getProjectionMatrix(), mainCam->getViewMatrix(),
                                             top, Vec2::ZERO, Vec2( mainLayer->GetVisibleSize().width,
                                                                               mainLayer->GetVisibleSize().height ) );

            if( topPos.y < 0 )
            {
//                log("================= y!");
                child->setPositionY( child->getPositionY() +
                                     verticalChildrenNumber * child->getBoundingBox().size.height );
            }
        }
    }
}

void InfiniteParallaxNode::AddBackgroundLayer(string fileName, int zOrder,
                                               Vec2 parallaxRatio, bool fillHorizontal , BackgroundLayerAlignment bga)
{
    auto layer = BackgroundLayer::create();
    mLayers.push_back( layer );

    auto firstBG = Sprite::create();
    firstBG->initWithFile( fileName );

    float width = firstBG->getBoundingBox().size.width;
    float height = firstBG->getBoundingBox().size.height;

    int verticalCount = ceil( fabs(mTopRight.y - mBottomLeft.y)/(height ) ) + 1 ;
    layer->SetVerticalChildrenNumber( verticalCount );
    int horizontalCount = 1;
    if( fillHorizontal )
    {

        horizontalCount = ceil( fabs(mTopRight.x - mBottomLeft.x)/width ) ;
    }

//    log( "vert %i hor %i", verticalCount, horizontalCount );
    float bgY = mBottomLeft.y + height/2;

    for( int i = 0 ; i < verticalCount; i++ )
    {
        float bgX = mBottomLeft.x + width/2;
        int coef = 1;
        if( bga == BGA_RIGHT )
        {
            bgX = mTopRight.x - width/2;
            coef = -1;
        }
        for( int j = 0; j < horizontalCount; j++)
        {
            auto bg = Sprite::create();
            bg->initWithFile( fileName );
            bg->setPosition( bgX, bgY );

            layer->addChild( bg );

            bgX = bgX + coef * width;

        }
        bgY += height;
    }
    addChild( layer, zOrder, parallaxRatio, Vec2(0,0) );
}
