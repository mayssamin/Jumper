#Introduction
This game is composed of four parts. Each part contains some levels and a special hero. Each hero will travel through obstacles and will fight  with its own special enemies. 

#Parts Overview

| Parts        | Part 1      | Part 2       | Part 3       | Part 4      |
| -------------|-------------| -------------|--------------|-------------|
| Hero      | -------------- | -------------- | -------------- |
| Total Levels      |  -------------- | -------------- | -------------- |
| Main Plant |  -------------- | -------------- | -------------- |
| Enemies |  -------------- | -------------- | -------------- |
| Power ups |  -------------- | -------------- | -------------- |
| Collectibles |  -------------- | -------------- | -------------- |
| Traps |  -------------- | -------------- | -------------- |
